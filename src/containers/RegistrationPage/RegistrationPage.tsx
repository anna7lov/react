import { RegistrationForm } from '../../components/forms/RegistrationForm/RegistrationForm';
import styles from '../../styles/shared.module.scss';

export const RegistrationPage = (): JSX.Element => {
  return (
    <div className={styles.pageContainer}>
      <RegistrationForm />
    </div>
  );
};
