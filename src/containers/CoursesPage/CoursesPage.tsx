import { DashboardLayout } from '../../components/layouts/dashboard/DashboardLayout/DashboardLayout';
import { CoursesView } from '../../components/views/CoursesView/CoursesView';

export const CoursesPage = (): JSX.Element => {
  return (
    <DashboardLayout title="Courses">
      <CoursesView />
    </DashboardLayout>
  );
};
