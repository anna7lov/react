import * as Yup from 'yup';

export const updateStudentFormSchema = Yup.object().shape({
  studentUpdateName: Yup.string()
    .max(25, 'Max length 25 characters')
    .required('Required'),

  studentUpdateSurname: Yup.string()
    .max(25, 'Max length 25 characters')
    .required('Required'),

  studentUpdateEmail: Yup.string()
    .email('Please enter a valid email')
    .required('Required'),

  studentUpdateAge: Yup.number().required('Required').min(14).max(99),
});

export type AddStudentFormSchema = Yup.InferType<
  typeof updateStudentFormSchema
>;
