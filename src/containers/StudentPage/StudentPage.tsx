import Select, { DropdownIndicatorProps, components } from 'react-select';
import { useEffect, useMemo, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Header } from '../../components/Header/Header';
import { Button } from '../../components/Button/Button';
import { InputField } from '../../components/InputField/InputField';
import { useFormik } from 'formik';
import { useDropzone } from 'react-dropzone';
import { useDispatch, useSelector } from 'react-redux';
import { selectStudent } from '../../redux/students/selectors';
import { getStudentThunk } from '../../redux/students/thunks';
import { getAllGroupsThunk } from '../../redux/groups/thunks';
import { selectGroups } from '../../redux/groups/selectors';
import { addImageApi, updateStudentApi } from '../../api/students.api';
import { AxiosError } from 'axios';
import { getAllCoursesThunk } from '../../redux/courses/thunks';
import { selectCourses } from '../../redux/courses/selectors';
import { ToastContainer, toast } from 'react-toastify';
import { customFormSelectStyles } from '../../styles/customFormSelectStyles';
import { RequestState } from '../../services/commonTypes';
import { Loading } from '../../components/Loading/Loading';
import { Error } from '../../components/Error/Error';
import { Course } from '../../services/coursesTypes';
import { updateStudentFormSchema } from './UpdateStudentFormSchema';
import 'react-toastify/dist/ReactToastify.css';
import styles from './StudentPage.module.scss';

export interface StudentUpdateFormValues {
  studentUpdateName: string;
  studentUpdateSurname: string;
  studentUpdateEmail: string;
  studentUpdateAge: string;
  studentUpdateCourse: Course[];
  studentUpdateGroup: number;
}

const DropdownIndicator = (props: DropdownIndicatorProps) => {
  return (
    <components.DropdownIndicator {...props}>
      <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path d="M12 14.5L17 9.5H7L12 14.5Z" fill="black" />
      </svg>
    </components.DropdownIndicator>
  );
};

export const StudentPage = (): JSX.Element => {
  const { id } = useParams();
  const dispatch = useDispatch();

  useEffect(() => {
    if (id) {
      dispatch(getStudentThunk(id));
      dispatch(getAllGroupsThunk());
      dispatch(getAllCoursesThunk());
    }
  }, [id, dispatch]);

  const student = useSelector(selectStudent);

  const currentStudent = useMemo(() => {
    if (!id) {
      return null;
    }
    return student[id] || null;
  }, [student, id]);

  const groups = useSelector(selectGroups);
  const courses = useSelector(selectCourses);

  const groupOptions = groups.map((group) => ({
    value: group.id,
    label: group.name,
  }));

  const courseOptions = courses.map((course) => ({
    value: course.id,
    label: course.name,
  }));

  const [uploadedFile, setUploadedFile] = useState<File>();

  const onDrop = (acceptedFiles: File[]) => {
    if (acceptedFiles.length > 0) {
      setUploadedFile(acceptedFiles[0]);
    }
  };

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    maxFiles: 1,
  });

  const onStudentUpdateFormSubmit = async (
    values: StudentUpdateFormValues,
  ): Promise<void> => {
    try {
      if (id) {
        const coursesList = values.studentUpdateCourse.map(
          (course) => course.id,
        );

        await updateStudentApi(id, {
          name: values.studentUpdateName,
          surname: values.studentUpdateSurname,
          email: values.studentUpdateEmail,
          age: +values.studentUpdateAge,
          groupId: values.studentUpdateGroup || null,
          coursesList: coursesList,
        });

        if (uploadedFile) {
          const formData = new FormData();
          formData.append('file', uploadedFile);
          await addImageApi(id, formData);
        }
      }
      window.location.reload();
    } catch (error) {
      const errorMessage =
        error instanceof AxiosError
          ? error.response?.data.message || 'Something went wrong'
          : 'Something went wrong';
      toast.error(errorMessage, {
        position: toast.POSITION.TOP_CENTER,
        autoClose: false,
      });
      console.log(errorMessage);
    }
  };

  const navigate = useNavigate();
  const handleBackClick = () => {
    navigate(-1);
  };

  const {
    values,
    errors,
    touched,
    isSubmitting,
    handleBlur,
    handleChange,
    handleSubmit,
    setValues,
  } = useFormik<StudentUpdateFormValues>({
    initialValues: {
      studentUpdateName: '',
      studentUpdateSurname: '',
      studentUpdateEmail: '',
      studentUpdateAge: '',
      studentUpdateCourse: [],
      studentUpdateGroup: 0,
    },
    validationSchema: updateStudentFormSchema,
    onSubmit: onStudentUpdateFormSubmit,
  });

  useEffect(() => {
    if (currentStudent) {
      const { info } = currentStudent;
      setValues({
        studentUpdateName: info?.name || '',
        studentUpdateSurname: info?.surname || '',
        studentUpdateEmail: info?.email || '',
        studentUpdateAge: info?.age || '',
        studentUpdateCourse: info?.courses || [],
        studentUpdateGroup: info?.groupId || 0,
      });
    }
  }, [currentStudent]);

  const handleGroupChange = (selectedOption: any) => {
    setValues({
      ...values,
      studentUpdateGroup: selectedOption?.value,
    });
  };

  const handleCoursesChange = (selectedOptions: any) => {
    setValues({
      ...values,
      studentUpdateCourse: selectedOptions.map((option: any) => ({
        id: option.value,
        label: option.label,
      })),
    });
  };

  if (currentStudent?.requestState === RequestState.Waiting) {
    return <Loading />;
  }

  if (
    currentStudent?.requestState === RequestState.Failure ||
    !id ||
    !currentStudent ||
    !currentStudent.info
  ) {
    return <Error title="Something went wrong" />;
  }

  return (
    <div className={styles.student}>
      <ToastContainer />
      <div className={styles.student__headerWrapper}>
        <Header isLogo={true} />
      </div>
      <main className={styles.student__main}>
        <button
          className={styles.student__backButton}
          onClick={handleBackClick}
          disabled={window.history.length === 1}
        >
          <span className={styles.student__buttonItem}>Back</span>
        </button>
        <form
          className={styles.student__form}
          noValidate
          onSubmit={handleSubmit}
        >
          <div className={styles.student__leftContent}>
            <h2 className={styles.student__title}>Personal information</h2>
            <div className={styles.student__uploaderContent}>
              <div className={styles.student__imageWrapper}>
                <img
                  src={
                    currentStudent?.info?.imagePath
                      ? `data:image/jpeg;base64,${currentStudent.info.imagePath}`
                      : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMgUT8jlhEYuFZp2zsJq8YOAzC3ISYJue6tg&usqp=CAU'
                  }
                  className={styles.student__image}
                  alt=""
                />
              </div>
              <div>
                <div {...getRootProps()}>
                  <input {...getInputProps()} />
                  <button
                    type="button"
                    className={styles.student__replaceButton}
                  >
                    Replace
                  </button>
                </div>
                <p className={styles.student__uploaderText}>
                  Must be a .jpg or .png file smaller than 10MB and at least
                  400px by 400px.
                </p>
              </div>
            </div>
            <InputField
              label="Name"
              type="text"
              id="registration-name"
              name="studentUpdateName"
              placeholder="Name"
              value={values.studentUpdateName}
              handleChange={handleChange}
              handleBlur={handleBlur}
              error={errors.studentUpdateName}
              touched={touched.studentUpdateName}
            />
            <InputField
              label="Surname"
              type="text"
              id="registration-surname"
              name="studentUpdateSurname"
              placeholder="Surname"
              value={values.studentUpdateSurname}
              handleChange={handleChange}
              handleBlur={handleBlur}
              error={errors.studentUpdateSurname}
              touched={touched.studentUpdateSurname}
            />
            <InputField
              label="Email"
              type="email"
              id="registration-email"
              name="studentUpdateEmail"
              placeholder="name@mail.com"
              value={values.studentUpdateEmail}
              handleChange={handleChange}
              handleBlur={handleBlur}
              error={errors.studentUpdateEmail}
              touched={touched.studentUpdateEmail}
            />
            <InputField
              label="Age"
              type="text"
              id="registration-email"
              name="studentUpdateAge"
              placeholder="Age"
              value={values.studentUpdateAge}
              handleChange={handleChange}
              handleBlur={handleBlur}
              error={errors.studentUpdateAge}
              touched={touched.studentUpdateAge}
            />
            <Button title="Save changes" isDisabled={isSubmitting} />
          </div>

          <div className={styles.student__rightContent}>
            <h2 className={styles.student__title}>Courses and Groups</h2>
            <label htmlFor="group-select" className={styles.select__label}>
              Group
            </label>
            <Select
              id="group-select"
              styles={customFormSelectStyles}
              options={groupOptions}
              className={styles.select}
              components={{ DropdownIndicator, IndicatorSeparator: null }}
              value={groupOptions.find(
                (option) => option.value === values.studentUpdateGroup,
              )}
              onChange={handleGroupChange}
            />
            <label htmlFor="course-select" className={styles.select__label}>
              Courses
            </label>
            <Select
              isMulti={true}
              id="course-select"
              styles={customFormSelectStyles}
              options={courseOptions}
              className={styles.select}
              components={{ DropdownIndicator, IndicatorSeparator: null }}
              value={courseOptions.filter((option) =>
                values.studentUpdateCourse.some(
                  (course) => course.id === option.value,
                ),
              )}
              onChange={handleCoursesChange}
            />
          </div>
        </form>
      </main>
    </div>
  );
};
