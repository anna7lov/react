import { DashboardLayout } from '../../components/layouts/dashboard/DashboardLayout/DashboardLayout';
import { LectorsView } from '../../components/views/LectorsView/LectorsView';

export const LectorsPage = (): JSX.Element => {
  return (
    <DashboardLayout title="Lectors">
      <LectorsView />
    </DashboardLayout>
  );
};
