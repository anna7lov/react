import { LoginForm } from '../../components/forms/LoginForm/LoginForm';
import styles from '../../styles/shared.module.scss';

export const LoginPage = (): JSX.Element => {
  return (
    <div className={styles.pageContainer}>
      <LoginForm />
    </div>
  );
};
