import { useSelector } from 'react-redux';
import { selectUser } from '../../redux/user/selectors';
import { DashboardLayout } from '../../components/layouts/dashboard/DashboardLayout/DashboardLayout';

export const DashboardPage = (): JSX.Element => {
  const user = useSelector(selectUser);
  return (
    <DashboardLayout title="Welcome">
      <div>Hello {user?.name}!</div>
    </DashboardLayout>
  );
};
