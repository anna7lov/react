import { DashboardLayout } from '../../components/layouts/dashboard/DashboardLayout/DashboardLayout';
import { StudentsView } from '../../components/views/StudentsView/StudentsView';

export const StudentsPage = (): JSX.Element => {
  return (
    <DashboardLayout title="Students">
      <StudentsView />
    </DashboardLayout>
  );
};
