import { ForgotPasswordForm } from '../../components/forms/ForgotPasswordForm/ForgotPasswordForm';
import styles from '../../styles/shared.module.scss';

export const ForgotPasswordPage = (): JSX.Element => {
  return (
    <div className={styles.pageContainer}>
      <ForgotPasswordForm />
    </div>
  );
};
