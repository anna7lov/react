import styles from '../../styles/shared.module.scss';

export const NotFoundPage = (): JSX.Element => {
  return <div className={styles.pageContainer}>Page not found</div>;
};
