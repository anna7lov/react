import { AuthLayout } from '../../../../components/layouts/auth/AuthLayout/AuthLayout';
import { LinkItem } from '../../../../components/LinkItem/LinkItem';

export const PasswordRecoverSuccess = (): JSX.Element => {
  return (
    <AuthLayout
      title="Password Changed"
      paragraph="You can use your new password to log into your account"
      paragraphCenter={true}
    >
      <LinkItem address="/" title="Log In" variant="filled" />
    </AuthLayout>
  );
};
