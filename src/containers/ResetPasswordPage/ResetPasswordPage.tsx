import { useSelector } from 'react-redux';
import { ResetPasswordForm } from '../../components/forms/ResetPasswordForm/ResetPasswordForm';
import { selectIsResetPasswordSuccess } from '../../redux/user/selectors';
import { PasswordRecoverSuccess } from './components/PasswordRecoverSuccess/PasswordRecoverSuccess';
import styles from '../../styles/shared.module.scss';

export const ResetPasswordPage = (): JSX.Element => {
  const isResetPasswordSuccess = useSelector(selectIsResetPasswordSuccess);

  const removeTokenFromURL = () => {
    const currentURL = window.location.href;
    const baseURL = currentURL.split('?')[0];
    window.history.pushState({}, document.title, baseURL);
  };

  if (isResetPasswordSuccess) {
    removeTokenFromURL();
  }

  return (
    <div className={styles.pageContainer}>
      {isResetPasswordSuccess ? (
        <PasswordRecoverSuccess />
      ) : (
        <ResetPasswordForm />
      )}
    </div>
  );
};
