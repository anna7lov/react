import { DashboardLayout } from '../../components/layouts/dashboard/DashboardLayout/DashboardLayout';
import { GroupsView } from '../../components/views/GroupsView/GroupsView';

export const GroupsPage = (): JSX.Element => {
  return (
    <DashboardLayout title="Groups">
      <GroupsView />
    </DashboardLayout>
  );
};
