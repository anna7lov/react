import { AxiosResponse } from 'axios';
import { apiCaller } from '../utils/apiCaller';
import { Lector } from '../services/lectorsTypes';

export const getAllLectorsApi = async (
  currentPage: number,
): Promise<AxiosResponse> => {
  return apiCaller({
    url: `lectors?page=${currentPage}`,
    method: 'GET',
  });
};

export const getLectorApi = async (id: number): Promise<AxiosResponse> => {
  return apiCaller({
    url: `lectors/${id}`,
    method: 'GET',
  });
};

export const addLectorApi = async (
  lector: Pick<Lector, 'name' | 'surname' | 'email'> & {
    password: string;
  },
): Promise<AxiosResponse> => {
  return apiCaller({
    url: 'lectors',
    method: 'POST',
    data: lector,
  });
};

export const updateLectorApi = async (
  lector: Pick<Lector, 'id' | 'name' | 'surname' | 'email'> & {
    password?: string;
  },
): Promise<AxiosResponse> => {
  const payload = lector.password
    ? {
        name: lector.name,
        surname: lector.surname,
        email: lector.email,
        password: lector.password,
      }
    : {
        name: lector.name,
        surname: lector.surname,
        email: lector.email,
      };

  return apiCaller({
    url: `lectors/${lector.id}`,
    method: 'PATCH',
    data: payload,
  });
};
