import { AxiosResponse } from 'axios';
import { apiCaller } from '../utils/apiCaller';
import { Course } from '../services/coursesTypes';

export const getAllCoursesApi = async (): Promise<AxiosResponse> => {
  return apiCaller({
    url: 'courses',
    method: 'GET',
  });
};

export const getCourseByIdApi = async (id: number): Promise<AxiosResponse> => {
  return apiCaller({
    url: `courses/${id}`,
    method: 'GET',
  });
};

export const addCourseApi = async (
  course: Pick<Course, 'name' | 'description'> & { hours: number },
): Promise<AxiosResponse> => {
  return apiCaller({
    url: 'courses',
    method: 'POST',
    data: course,
  });
};

export const updateCourseByIdApi = async (
  course: Pick<Course, 'id' | 'name' | 'description' | 'hours'>,
): Promise<AxiosResponse> => {
  return apiCaller({
    url: `courses/${course.id}`,
    method: 'PATCH',
    data: {
      name: course.name,
      description: course.description,
      hours: +course.hours,
    },
  });
};
