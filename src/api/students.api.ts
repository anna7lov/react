import { AxiosResponse } from 'axios';
import { apiCaller } from '../utils/apiCaller';
import { Student } from '../services/studentsTypes';

export const getAllStudentsApi = async (
  sort: string,
  order: string,
  name: string,
  search: string,
): Promise<AxiosResponse> => {
  return apiCaller({
    url: `students?sort=${sort}&order=${order}&${search}=${name}`,
    method: 'GET',
  });
};

export const getStudentApi = async (id: string): Promise<AxiosResponse> => {
  return apiCaller({
    url: `students/${id}`,
    method: 'GET',
  });
};

export const addStudentApi = async (
  student: Pick<Student, 'name' | 'surname' | 'email'> & {
    age: number;
    groupId: number | null;
    coursesList: number[];
  },
): Promise<AxiosResponse> => {
  return apiCaller({
    url: 'students',
    method: 'POST',
    data: student,
  });
};

export const updateStudentApi = async (
  id: string,
  payload: Pick<Student, 'name' | 'surname' | 'email'> & {
    age: number;
    groupId: number | null;
    coursesList: number[];
  },
): Promise<AxiosResponse> => {
  return apiCaller({
    url: `students/${id}`,
    method: 'PATCH',
    data: payload,
  });
};

export const addImageApi = async (
  id: string,
  payload: FormData,
): Promise<AxiosResponse> => {
  return apiCaller({
    url: `students/${id}/image`,
    method: 'PATCH',
    data: payload,
  });
};
