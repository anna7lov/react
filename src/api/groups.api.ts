import { AxiosResponse } from 'axios';
import { apiCaller } from '../utils/apiCaller';
import { Group } from '../services/groupsTypes';

export const getAllGroupsApi = async (): Promise<AxiosResponse> => {
  return apiCaller({
    url: 'groups',
    method: 'GET',
  });
};

export const getGroupApi = async (id: number): Promise<AxiosResponse> => {
  return apiCaller({
    url: `groups/${id}`,
    method: 'GET',
  });
};

export const addGroupApi = async (
  group: Pick<Group, 'name'>,
): Promise<AxiosResponse> => {
  return apiCaller({
    url: 'groups',
    method: 'POST',
    data: group,
  });
};

export const updateGroupByIdApi = async (
  group: Pick<Group, 'id' | 'name'>,
): Promise<AxiosResponse> => {
  return apiCaller({
    url: `groups/${group.id}`,
    method: 'PATCH',
    data: { name: group.name },
  });
};
