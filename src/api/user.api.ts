import { AxiosResponse } from 'axios';
import { apiCaller } from '../utils/apiCaller';
import {
  UserForgotPasswordPayload,
  UserResetPasswordPayload,
  UserSignInPayload,
  UserSignUpPayload,
} from '../services/userTypes';

export const getUserApi = async (): Promise<AxiosResponse> => {
  return apiCaller({
    url: 'lectors/me',
    method: 'GET',
  });
};

export const signInApi = async (
  payload: UserSignInPayload,
): Promise<AxiosResponse> => {
  return apiCaller({
    url: 'auth/sign-in',
    method: 'POST',
    data: payload,
  });
};

export const signUpApi = async (
  payload: UserSignUpPayload,
): Promise<AxiosResponse> => {
  return apiCaller({
    url: 'auth/sign-up',
    method: 'POST',
    data: payload,
  });
};

export const forgotPasswordApi = async (
  payload: UserForgotPasswordPayload,
): Promise<AxiosResponse> => {
  return apiCaller({
    url: 'auth/forgot-password',
    method: 'POST',
    data: payload,
  });
};

export const resetPasswordApi = async (
  payload: UserResetPasswordPayload,
): Promise<AxiosResponse> => {
  return apiCaller({
    url: 'auth/reset-password',
    method: 'POST',
    data: payload,
  });
};
