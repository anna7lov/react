import { ActionType } from 'typesafe-actions';
import * as userActions from './user/actions';
import * as lectorsActions from './lectors/actions';
import * as studentsActions from './students/actions';
import * as groupsActions from './groups/actions';
import * as coursesActions from './courses/actions';

const allActions = {
  userActions,
  lectorsActions,
  studentsActions,
  groupsActions,
  coursesActions,
};

export type GlobalAppActions = ActionType<typeof allActions>;
