import { getType } from 'typesafe-actions';
import { GlobalAppActions } from '../actions';
import { RequestState } from '../../services/commonTypes';
import { getAllCoursesAsyncAction, getCourseAsyncAction } from './actions';
import { Course } from '../../services/coursesTypes';

export interface CoursesState {
  courses: Course[];
  coursesRequestState: RequestState;
  coursesError: string | null;
  course: Course | null;
  courseRequestState: RequestState;
  courseError: string | null;
}

const initialState: CoursesState = {
  courses: [],
  coursesRequestState: RequestState.Unset,
  coursesError: null,
  course: null,
  courseRequestState: RequestState.Unset,
  courseError: null,
};

export const reducer = (
  state = initialState,
  action: GlobalAppActions,
): CoursesState => {
  switch (action.type) {
    case getType(getAllCoursesAsyncAction.request): {
      return {
        ...state,
        coursesRequestState: RequestState.Waiting,
        coursesError: null,
      };
    }

    case getType(getAllCoursesAsyncAction.success): {
      return {
        ...state,
        courses: action.payload.courses,
        coursesRequestState: RequestState.Success,
        coursesError: null,
      };
    }

    case getType(getAllCoursesAsyncAction.failure): {
      return {
        ...state,
        coursesRequestState: RequestState.Failure,
        coursesError: action.payload.error,
      };
    }

    case getType(getCourseAsyncAction.request): {
      return {
        ...state,
        courseRequestState: RequestState.Waiting,
        courseError: null,
      };
    }

    case getType(getCourseAsyncAction.success): {
      return {
        ...state,
        course: action.payload.course,
        courseRequestState: RequestState.Success,
        courseError: null,
      };
    }

    case getType(getCourseAsyncAction.failure): {
      return {
        ...state,
        courseRequestState: RequestState.Failure,
        courseError: action.payload.error,
      };
    }

    default:
      return state;
  }
};
