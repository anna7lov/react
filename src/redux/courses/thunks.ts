import { getAllCoursesAsyncAction, getCourseAsyncAction } from './actions';
import { AppDispatch } from '../index';
import { ThunkAction } from 'redux-thunk';
import { GlobalAppState } from '../rootReducer';
import { GlobalAppActions } from '../actions';
import { AxiosError } from 'axios';
import { getAllCoursesApi, getCourseByIdApi } from '../../api/courses.api';

export type ThunkAppType = ThunkAction<
  Promise<void>,
  GlobalAppState,
  undefined,
  GlobalAppActions
>;

export const getAllCoursesThunk = () => async (dispatch: AppDispatch) => {
  dispatch(getAllCoursesAsyncAction.request());
  try {
    const courses = await getAllCoursesApi();
    dispatch(getAllCoursesAsyncAction.success({ courses: courses.data }));
  } catch (error) {
    const errorMessage =
      error instanceof AxiosError
        ? error.response?.data.message || 'Something went wrong'
        : 'Something went wrong';
    dispatch(getAllCoursesAsyncAction.failure({ error: errorMessage }));
  }
};

export const getCourseThunk = (id: number) => async (dispatch: AppDispatch) => {
  dispatch(getCourseAsyncAction.request());
  try {
    const course = await getCourseByIdApi(id);
    dispatch(getCourseAsyncAction.success({ course: course.data }));
  } catch (error) {
    const errorMessage =
      error instanceof AxiosError
        ? error.response?.data.message || 'Something went wrong'
        : 'Something went wrong';
    dispatch(getCourseAsyncAction.failure({ error: errorMessage }));
  }
};
