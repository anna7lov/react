import { createAsyncAction } from 'typesafe-actions';
import { Course } from '../../services/coursesTypes';

export enum CoursesActions {
  GET_ALL_COURSES_REQUEST = '@courses/GET_ALL_COURSES_REQUEST',
  GET_ALL_COURSES_SUCCESS = '@courses/GET_ALL_COURSES_SUCCESS',
  GET_ALL_COURSES_FAILURE = '@courses/GET_ALL_COURSES_FAILURE',

  GET_COURSE_REQUEST = '@courses/GET_COURSE_REQUEST',
  GET_COURSE_SUCCESS = '@courses/GET_COURSE_SUCCESS',
  GET_COURSE_FAILURE = '@courses/GET_COURSE_FAILURE',
}

export const getAllCoursesAsyncAction = createAsyncAction(
  CoursesActions.GET_ALL_COURSES_REQUEST,
  CoursesActions.GET_ALL_COURSES_SUCCESS,
  CoursesActions.GET_ALL_COURSES_FAILURE,
)<undefined, { courses: Course[] }, { error: string }>();

export const getCourseAsyncAction = createAsyncAction(
  CoursesActions.GET_COURSE_REQUEST,
  CoursesActions.GET_COURSE_SUCCESS,
  CoursesActions.GET_COURSE_FAILURE,
)<undefined, { course: Course }, { error: string }>();
