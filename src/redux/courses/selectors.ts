import { RequestState } from '../../services/commonTypes';
import { Course } from '../../services/coursesTypes';
import { GlobalAppState } from '../rootReducer';

export const selectCourses = (state: GlobalAppState): Course[] =>
  state.courses.courses;

export const selectAreCoursesLoading = (state: GlobalAppState): boolean =>
  state.courses.coursesRequestState === RequestState.Waiting;

export const selectCoursesError = (state: GlobalAppState): string | null =>
  state.courses.coursesError;

export const selectCourse = (state: GlobalAppState): Course =>
  state.courses.course as Course;

export const selectIsCourseLoading = (state: GlobalAppState): boolean =>
  state.courses.courseRequestState === RequestState.Waiting;

export const selectCourseError = (state: GlobalAppState): string | null =>
  state.courses.courseError;
