import { combineReducers } from 'redux';
import { reducer as userReducer, UserState } from './user/reducer';
import { reducer as lectorsReducer, LectorsState } from './lectors/reducer';
import { reducer as studentsReducer, StudentsState } from './students/reducer';
import { reducer as groupsReducer, GroupsState } from './groups/reducer';
import { reducer as coursesReducer, CoursesState } from './courses/reducer';

export interface GlobalAppState {
  user: UserState;
  lectors: LectorsState;
  students: StudentsState;
  groups: GroupsState;
  courses: CoursesState;
}

export const rootReducer = combineReducers<GlobalAppState>({
  user: userReducer,
  lectors: lectorsReducer,
  students: studentsReducer,
  groups: groupsReducer,
  courses: coursesReducer,
});
