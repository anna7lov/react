import {
  legacy_createStore as createStore,
  applyMiddleware,
  Store,
} from 'redux';
import { composeWithDevTools } from '@redux-devtools/extension';
import thunk from 'redux-thunk';
import { rootReducer, GlobalAppState } from './rootReducer';
import type {} from 'redux-thunk/extend-redux';

export const store: Store<GlobalAppState> = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk)),
);

export type AppDispatch = typeof store.dispatch;
