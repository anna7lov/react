import { RequestState } from '../../services/commonTypes';
import { Group } from '../../services/groupsTypes';
import { GlobalAppState } from '../rootReducer';

export const selectGroups = (state: GlobalAppState): Group[] =>
  state.groups.groups;

export const selectAreGroupsLoading = (state: GlobalAppState): boolean =>
  state.groups.groupsRequestState === RequestState.Waiting;

export const selectGroupsError = (state: GlobalAppState): string | null =>
  state.groups.groupsError;

export const selectGroup = (state: GlobalAppState): Group =>
  state.groups.group as Group;

export const selectIsGroupLoading = (state: GlobalAppState): boolean =>
  state.groups.groupRequestState === RequestState.Waiting;

export const selectGroupError = (state: GlobalAppState): string | null =>
  state.groups.groupError;
