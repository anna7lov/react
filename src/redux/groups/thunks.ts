import { getAllGroupsAsyncAction, getGroupAsyncAction } from './actions';
import { AppDispatch } from '../index';
import { ThunkAction } from 'redux-thunk';
import { GlobalAppState } from '../rootReducer';
import { GlobalAppActions } from '../actions';
import { AxiosError } from 'axios';
import { getAllGroupsApi, getGroupApi } from '../../api/groups.api';

export type ThunkAppType = ThunkAction<
  Promise<void>,
  GlobalAppState,
  undefined,
  GlobalAppActions
>;

export const getAllGroupsThunk = () => async (dispatch: AppDispatch) => {
  dispatch(getAllGroupsAsyncAction.request());
  try {
    const groups = await getAllGroupsApi();
    dispatch(getAllGroupsAsyncAction.success({ groups: groups.data }));
  } catch (error) {
    const errorMessage =
      error instanceof AxiosError
        ? error.response?.data.message || 'Something went wrong'
        : 'Something went wrong';
    dispatch(getAllGroupsAsyncAction.failure({ error: errorMessage }));
  }
};

export const getGroupThunk = (id: number) => async (dispatch: AppDispatch) => {
  dispatch(getGroupAsyncAction.request());
  try {
    const group = await getGroupApi(id);
    dispatch(getGroupAsyncAction.success({ group: group.data }));
  } catch (error) {
    const errorMessage =
      error instanceof AxiosError
        ? error.response?.data.message || 'Something went wrong'
        : 'Something went wrong';
    dispatch(getGroupAsyncAction.failure({ error: errorMessage }));
  }
};
