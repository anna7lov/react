import { getType } from 'typesafe-actions';
import { GlobalAppActions } from '../actions';
import { RequestState } from '../../services/commonTypes';
import { getAllGroupsAsyncAction, getGroupAsyncAction } from './actions';
import { Group } from '../../services/groupsTypes';

export interface GroupsState {
  groups: Group[];
  groupsRequestState: RequestState;
  groupsError: string | null;
  group: Group | null;
  groupRequestState: RequestState;
  groupError: string | null;
}

const initialState: GroupsState = {
  groups: [],
  groupsRequestState: RequestState.Unset,
  groupsError: null,
  group: null,
  groupRequestState: RequestState.Unset,
  groupError: null,
};

export const reducer = (
  state = initialState,
  action: GlobalAppActions,
): GroupsState => {
  switch (action.type) {
    case getType(getAllGroupsAsyncAction.request): {
      return {
        ...state,
        groupsRequestState: RequestState.Waiting,
        groupsError: null,
      };
    }

    case getType(getAllGroupsAsyncAction.success): {
      return {
        ...state,
        groups: action.payload.groups,
        groupsRequestState: RequestState.Success,
        groupsError: null,
      };
    }

    case getType(getAllGroupsAsyncAction.failure): {
      return {
        ...state,
        groupsRequestState: RequestState.Failure,
        groupsError: action.payload.error,
      };
    }

    case getType(getGroupAsyncAction.request): {
      return {
        ...state,
        groupRequestState: RequestState.Waiting,
        groupError: null,
      };
    }

    case getType(getGroupAsyncAction.success): {
      return {
        ...state,
        group: action.payload.group,
        groupRequestState: RequestState.Success,
        groupError: null,
      };
    }

    case getType(getGroupAsyncAction.failure): {
      return {
        ...state,
        groupRequestState: RequestState.Failure,
        groupError: action.payload.error,
      };
    }

    default:
      return state;
  }
};
