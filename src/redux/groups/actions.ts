import { createAsyncAction } from 'typesafe-actions';
import { Group } from '../../services/groupsTypes';

export enum GroupsActions {
  GET_ALL_GROUPS_REQUEST = '@groups/GET_ALL_GROUPS_REQUEST',
  GET_ALL_GROUPS_SUCCESS = '@groups/GET_ALL_GROUPS_SUCCESS',
  GET_ALL_GROUPS_FAILURE = '@groups/GET_ALL_GROUPS_FAILURE',

  GET_GROUP_REQUEST = '@groups/GET_GROUP_REQUEST',
  GET_GROUP_SUCCESS = '@groups/GET_GROUP_SUCCESS',
  GET_GROUP_FAILURE = '@groups/GET_GROUP_FAILURE',
}

export const getAllGroupsAsyncAction = createAsyncAction(
  GroupsActions.GET_ALL_GROUPS_REQUEST,
  GroupsActions.GET_ALL_GROUPS_SUCCESS,
  GroupsActions.GET_ALL_GROUPS_FAILURE,
)<undefined, { groups: Group[] }, { error: string }>();

export const getGroupAsyncAction = createAsyncAction(
  GroupsActions.GET_GROUP_REQUEST,
  GroupsActions.GET_GROUP_SUCCESS,
  GroupsActions.GET_GROUP_FAILURE,
)<undefined, { group: Group }, { error: string }>();
