import { createAction, createAsyncAction } from 'typesafe-actions';
import { User } from '../../services/userTypes';

export enum UserActions {
  SIGN_IN_REQUEST = '@user/SIGN_IN_REQUEST',
  SIGN_IN_SUCCESS = '@user/SIGN_IN_SUCCESS',
  SIGN_IN_FAILURE = '@user/SIGN_IN_FAILURE',

  SET_USER_REQUEST = '@user/SET_USER_REQUEST',
  SET_USER_SUCCESS = '@user/SET_USER_SUCCESS',
  SET_USER_FAILURE = '@user/SET_USER_FAILURE',

  STOP_LOADING = '@user/STOP_LOADING',
  LOGOUT_USER = '@user/LOGOUT_USER',

  SIGN_UP_REQUEST = '@user/SIGN_UP_REQUEST',
  SIGN_UP_SUCCESS = '@user/SIGN_UP_SUCCESS',
  SIGN_UP_FAILURE = '@user/SIGN_UP_FAILURE',

  FORGOT_PASSWORD_REQUEST = '@user/FORGOT_PASSWORD_REQUEST',
  FORGOT_PASSWORD_SUCCESS = '@user/FORGOT_PASSWORD_SUCCESS',
  FORGOT_PASSWORD_FAILURE = '@user/FORGOT_PASSWORD_FAILURE',

  RESET_PASSWORD_REQUEST = '@user/RESET_PASSWORD_REQUEST',
  RESET_PASSWORD_SUCCESS = '@user/RESET_PASSWORD_SUCCESS',
  RESET_PASSWORD_FAILURE = '@user/RESET_PASSWORD_FAILURE',
}

export const signInAsyncAction = createAsyncAction(
  UserActions.SIGN_IN_REQUEST,
  UserActions.SIGN_IN_SUCCESS,
  UserActions.SIGN_IN_FAILURE,
)<undefined, undefined, { error: string }>();

export const setUserAsyncAction = createAsyncAction(
  UserActions.SET_USER_REQUEST,
  UserActions.SET_USER_SUCCESS,
  UserActions.SET_USER_FAILURE,
)<undefined, { user: User }, { error: string }>();

export const stopUserLoadingAction = createAction(UserActions.STOP_LOADING)();
export const logOutAction = createAction(UserActions.LOGOUT_USER)();

export const signUpAsyncAction = createAsyncAction(
  UserActions.SIGN_UP_REQUEST,
  UserActions.SIGN_UP_SUCCESS,
  UserActions.SIGN_UP_FAILURE,
)<undefined, undefined, { error: string }>();

export const forgotPasswordAsyncAction = createAsyncAction(
  UserActions.FORGOT_PASSWORD_REQUEST,
  UserActions.FORGOT_PASSWORD_SUCCESS,
  UserActions.FORGOT_PASSWORD_FAILURE,
)<undefined, undefined, { error: string }>();

export const resetPasswordAsyncAction = createAsyncAction(
  UserActions.RESET_PASSWORD_REQUEST,
  UserActions.RESET_PASSWORD_SUCCESS,
  UserActions.RESET_PASSWORD_FAILURE,
)<undefined, undefined, { error: string }>();
