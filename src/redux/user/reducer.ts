import { getType } from 'typesafe-actions';
import { GlobalAppActions } from '../actions';
import { User } from '../../services/userTypes';
import {
  stopUserLoadingAction,
  logOutAction,
  setUserAsyncAction,
  signInAsyncAction,
  signUpAsyncAction,
  forgotPasswordAsyncAction,
  resetPasswordAsyncAction,
} from './actions';
import { RequestState } from '../../services/commonTypes';

export interface UserState {
  user: User | null;
  isUserLoading: boolean;
  signInError: string | null;
  signUpError: string | null;
  setUserError: string | null;
  forgotPasswordRequestState: RequestState;
  forgotPasswordError: string | null;
  resetPasswordRequestState: RequestState;
  resetPasswordError: string | null;
}

const initialState: UserState = {
  user: null,
  isUserLoading: true,
  signInError: null,
  signUpError: null,
  setUserError: null,
  forgotPasswordRequestState: RequestState.Unset,
  forgotPasswordError: null,
  resetPasswordRequestState: RequestState.Unset,
  resetPasswordError: null,
};

export const reducer = (
  state = initialState,
  action: GlobalAppActions,
): UserState => {
  switch (action.type) {
    case getType(signInAsyncAction.request): {
      return {
        ...state,
        signInError: null,
      };
    }

    case getType(signInAsyncAction.success): {
      return {
        ...state,
        signInError: null,
      };
    }

    case getType(signInAsyncAction.failure): {
      return {
        ...state,
        signInError: action.payload.error,
      };
    }

    case getType(setUserAsyncAction.request): {
      return {
        ...state,
        isUserLoading: true,
        setUserError: null,
      };
    }

    case getType(setUserAsyncAction.success): {
      return {
        ...state,
        user: action.payload.user,
        isUserLoading: false,
        setUserError: null,
      };
    }

    case getType(setUserAsyncAction.failure): {
      return {
        ...state,
        isUserLoading: false,
        setUserError: action.payload.error,
      };
    }

    case getType(stopUserLoadingAction):
      return {
        ...state,
        isUserLoading: false,
      };

    case getType(logOutAction):
      return {
        ...state,
        user: null,
      };

    case getType(signUpAsyncAction.request): {
      return {
        ...state,
        signUpError: null,
      };
    }

    case getType(signUpAsyncAction.success): {
      return {
        ...state,
        signUpError: null,
      };
    }

    case getType(signUpAsyncAction.failure): {
      return {
        ...state,
        signUpError: action.payload.error,
      };
    }

    case getType(forgotPasswordAsyncAction.request): {
      return {
        ...state,
        forgotPasswordRequestState: RequestState.Waiting,
        forgotPasswordError: null,
      };
    }

    case getType(forgotPasswordAsyncAction.success): {
      return {
        ...state,
        forgotPasswordRequestState: RequestState.Success,
        forgotPasswordError: null,
      };
    }

    case getType(forgotPasswordAsyncAction.failure): {
      return {
        ...state,
        forgotPasswordRequestState: RequestState.Failure,
        forgotPasswordError: action.payload.error,
      };
    }

    case getType(resetPasswordAsyncAction.request): {
      return {
        ...state,
        resetPasswordRequestState: RequestState.Waiting,
        resetPasswordError: null,
      };
    }

    case getType(resetPasswordAsyncAction.success): {
      return {
        ...state,
        resetPasswordRequestState: RequestState.Success,
        resetPasswordError: null,
      };
    }

    case getType(resetPasswordAsyncAction.failure): {
      return {
        ...state,
        resetPasswordRequestState: RequestState.Failure,
        resetPasswordError: action.payload.error,
      };
    }

    default:
      return state;
  }
};
