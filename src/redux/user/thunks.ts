import {
  forgotPasswordAsyncAction,
  resetPasswordAsyncAction,
  setUserAsyncAction,
  signInAsyncAction,
  signUpAsyncAction,
} from './actions';
import { AppDispatch } from '../index';
import { ThunkAction } from 'redux-thunk';
import { GlobalAppState } from '../rootReducer';
import { GlobalAppActions } from '../actions';
import {
  UserForgotPasswordPayload,
  UserResetPasswordPayload,
  UserSignInPayload,
  UserSignUpPayload,
} from '../../services/userTypes';
import {
  forgotPasswordApi,
  getUserApi,
  resetPasswordApi,
  signInApi,
  signUpApi,
} from '../../api/user.api';
import { AxiosError } from 'axios';

export type ThunkAppType = ThunkAction<
  Promise<void>,
  GlobalAppState,
  undefined,
  GlobalAppActions
>;

export const getUserThunk = () => async (dispatch: AppDispatch) => {
  dispatch(setUserAsyncAction.request());
  try {
    const user = await getUserApi();
    dispatch(setUserAsyncAction.success({ user: user.data }));
  } catch (error) {
    localStorage.removeItem('token');
    const errorMessage =
      error instanceof AxiosError
        ? error.response?.data.message || 'Something went wrong'
        : 'Something went wrong';
    dispatch(setUserAsyncAction.failure({ error: errorMessage }));
  }
};

export const signInThunk =
  (payload: UserSignInPayload): ThunkAppType =>
  async (dispatch: AppDispatch) => {
    dispatch(signInAsyncAction.request());
    try {
      const response = await signInApi(payload);
      if (response.data.accessToken) {
        localStorage.setItem('token', response.data.accessToken);
        dispatch(signInAsyncAction.success());
        dispatch(getUserThunk());
      }
    } catch (error) {
      const errorMessage =
        error instanceof AxiosError
          ? error.response?.data.message || 'Something went wrong'
          : 'Something went wrong';
      dispatch(signInAsyncAction.failure({ error: errorMessage }));
    }
  };

export const signUpThunk =
  (payload: UserSignUpPayload): ThunkAppType =>
  async (dispatch: AppDispatch) => {
    dispatch(signUpAsyncAction.request());
    try {
      const response = await signUpApi(payload);
      if (response.data.accessToken) {
        localStorage.setItem('token', response.data.accessToken);
        dispatch(signUpAsyncAction.success());
        dispatch(getUserThunk());
      }
    } catch (error) {
      const errorMessage =
        error instanceof AxiosError
          ? error.response?.data.message || 'Something went wrong'
          : 'Something went wrong';
      dispatch(signUpAsyncAction.failure({ error: errorMessage }));
    }
  };

export const forgotPasswordThunk =
  (payload: UserForgotPasswordPayload): ThunkAppType =>
  async (dispatch: AppDispatch) => {
    dispatch(forgotPasswordAsyncAction.request());
    try {
      await forgotPasswordApi(payload);
      dispatch(forgotPasswordAsyncAction.success());
    } catch (error) {
      const errorMessage =
        error instanceof AxiosError
          ? error.response?.data.message || 'Something went wrong'
          : 'Something went wrong';
      dispatch(forgotPasswordAsyncAction.failure({ error: errorMessage }));
    }
  };

export const resetPasswordThunk =
  (payload: UserResetPasswordPayload): ThunkAppType =>
  async (dispatch: AppDispatch) => {
    dispatch(resetPasswordAsyncAction.request());
    try {
      await resetPasswordApi(payload);
      dispatch(resetPasswordAsyncAction.success());
    } catch (error) {
      const errorMessage =
        error instanceof AxiosError
          ? error.response?.data.message || 'Something went wrong'
          : 'Something went wrong';
      dispatch(resetPasswordAsyncAction.failure({ error: errorMessage }));
    }
  };
