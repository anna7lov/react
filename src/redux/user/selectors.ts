import { RequestState } from '../../services/commonTypes';
import { User } from '../../services/userTypes';
import { GlobalAppState } from '../rootReducer';

export const selectUser = (state: GlobalAppState): User | null =>
  state.user.user;

export const selectIsUserLoading = (state: GlobalAppState): boolean =>
  state.user.isUserLoading;

export const selectisUserSignInFailed = (
  state: GlobalAppState,
): string | null => state.user.signInError;

export const selectisUserSetFailed = (state: GlobalAppState): string | null =>
  state.user.setUserError;

export const selectisUserSignUpFailed = (
  state: GlobalAppState,
): string | null => state.user.signUpError;

export const selectIsForgotPasswordLoading = (state: GlobalAppState): boolean =>
  state.user.forgotPasswordRequestState === RequestState.Waiting;

export const selectIsForgotPasswordSuccess = (state: GlobalAppState): boolean =>
  state.user.forgotPasswordRequestState === RequestState.Success;

export const selectIsForgotPasswordFailed = (
  state: GlobalAppState,
): string | null => state.user.forgotPasswordError;

export const selectIsResetPasswordSuccess = (state: GlobalAppState): boolean =>
  state.user.resetPasswordRequestState === RequestState.Success;

export const selectIsResetPasswordFailed = (
  state: GlobalAppState,
): string | null => state.user.resetPasswordError;
