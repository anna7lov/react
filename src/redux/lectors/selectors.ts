import { RequestState } from '../../services/commonTypes';
import { Lector } from '../../services/lectorsTypes';
import { GlobalAppState } from '../rootReducer';

export const selectLectors = (state: GlobalAppState): Lector[] =>
  state.lectors.list?.lectors || [];

export const selectLectorsCount = (state: GlobalAppState): number =>
  state.lectors.list?.total as number;

export const selectAreLectorsLoading = (state: GlobalAppState): boolean =>
  state.lectors.lectorsRequestState === RequestState.Waiting;

export const selectLectorsError = (state: GlobalAppState): string | null =>
  state.lectors.lectorsError;

export const selectLector = (state: GlobalAppState): Lector =>
  state.lectors.lector as Lector;

export const selectIsLectorLoading = (state: GlobalAppState): boolean =>
  state.lectors.lectorRequestState === RequestState.Waiting;

export const selectLectorError = (state: GlobalAppState): string | null =>
  state.lectors.lectorError;

export const selectPage = (state: GlobalAppState) => state.lectors.page;
