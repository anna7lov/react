import { createAction, createAsyncAction } from 'typesafe-actions';
import { Lector } from '../../services/lectorsTypes';

export enum LectorsActions {
  GET_ALL_LECTORS_REQUEST = '@lectors/GET_ALL_LECTORS_REQUEST',
  GET_ALL_LECTORS_SUCCESS = '@lectors/GET_ALL_LECTORS_SUCCESS',
  GET_ALL_LECTORS_FAILURE = '@lectors/GET_ALL_LECTORS_FAILURE',

  GET_LECTOR_REQUEST = '@lectors/GET_LECTOR_REQUEST',
  GET_LECTOR_SUCCESS = '@lectors/GET_LECTOR_SUCCESS',
  GET_LECTOR_FAILURE = '@lectors/GET_LECTOR_FAILURE',

  PAGINATE_LECTORS = '@lectors/PAGINATE_LECTORS',
}

export const getAllLectorsAsyncAction = createAsyncAction(
  LectorsActions.GET_ALL_LECTORS_REQUEST,
  LectorsActions.GET_ALL_LECTORS_SUCCESS,
  LectorsActions.GET_ALL_LECTORS_FAILURE,
)<
  undefined,
  {
    list: {
      lectors: Lector[];
      total: number;
    };
  },
  { error: string }
>();

export const getLectorAsyncAction = createAsyncAction(
  LectorsActions.GET_LECTOR_REQUEST,
  LectorsActions.GET_LECTOR_SUCCESS,
  LectorsActions.GET_LECTOR_FAILURE,
)<undefined, { lector: Lector }, { error: string }>();

export const paginateLectors = createAction(
  LectorsActions.PAGINATE_LECTORS,
  ({ page }: { page: number }) => ({
    page,
  }),
)();
