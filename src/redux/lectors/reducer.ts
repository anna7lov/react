import { getType } from 'typesafe-actions';
import { GlobalAppActions } from '../actions';
import { Lector } from '../../services/lectorsTypes';
import { RequestState } from '../../services/commonTypes';
import {
  getAllLectorsAsyncAction,
  getLectorAsyncAction,
  paginateLectors,
} from './actions';

export interface LectorsState {
  list: {
    lectors: Lector[];
    total: number;
  } | null;
  lectorsRequestState: RequestState;
  lectorsError: string | null;
  lector: Lector | null;
  lectorRequestState: RequestState;
  lectorError: string | null;
  page: number;
}

const initialState: LectorsState = {
  list: null,
  lectorsRequestState: RequestState.Unset,
  lectorsError: null,
  lector: null,
  lectorRequestState: RequestState.Unset,
  lectorError: null,
  page: 0,
};

export const reducer = (
  state = initialState,
  action: GlobalAppActions,
): LectorsState => {
  switch (action.type) {
    case getType(getAllLectorsAsyncAction.request): {
      return {
        ...state,
        lectorsRequestState: RequestState.Waiting,
        lectorsError: null,
      };
    }

    case getType(getAllLectorsAsyncAction.success): {
      return {
        ...state,
        list: {
          lectors: action.payload.list.lectors,
          total: action.payload.list.total,
        },
        lectorsRequestState: RequestState.Success,
        lectorsError: null,
      };
    }

    case getType(getAllLectorsAsyncAction.failure): {
      return {
        ...state,
        lectorsRequestState: RequestState.Failure,
        lectorsError: action.payload.error,
      };
    }

    case getType(getLectorAsyncAction.request): {
      return {
        ...state,
        lectorRequestState: RequestState.Waiting,
        lectorError: null,
      };
    }

    case getType(getLectorAsyncAction.success): {
      return {
        ...state,
        lector: action.payload.lector,
        lectorRequestState: RequestState.Success,
        lectorError: null,
      };
    }

    case getType(getLectorAsyncAction.failure): {
      return {
        ...state,
        lectorRequestState: RequestState.Failure,
        lectorError: action.payload.error,
      };
    }

    case getType(paginateLectors):
      return {
        ...state,
        page: action.payload.page,
      };

    default:
      return state;
  }
};
