import { getAllLectorsAsyncAction, getLectorAsyncAction } from './actions';
import { AppDispatch } from '../index';
import { ThunkAction } from 'redux-thunk';
import { GlobalAppState } from '../rootReducer';
import { GlobalAppActions } from '../actions';
import { AxiosError } from 'axios';
import { getAllLectorsApi, getLectorApi } from '../../api/lectors.api';

export type ThunkAppType = ThunkAction<
  Promise<void>,
  GlobalAppState,
  undefined,
  GlobalAppActions
>;

export const getAllLectorsThunk =
  (currentPage: number) => async (dispatch: AppDispatch) => {
    dispatch(getAllLectorsAsyncAction.request());
    try {
      const lectors = await getAllLectorsApi(currentPage);
      dispatch(getAllLectorsAsyncAction.success({ list: lectors.data }));
    } catch (error) {
      const errorMessage =
        error instanceof AxiosError
          ? error.response?.data.message || 'Something went wrong'
          : 'Something went wrong';
      dispatch(getAllLectorsAsyncAction.failure({ error: errorMessage }));
    }
  };

export const getLectorThunk = (id: number) => async (dispatch: AppDispatch) => {
  dispatch(getLectorAsyncAction.request());
  try {
    const lector = await getLectorApi(id);
    dispatch(getLectorAsyncAction.success({ lector: lector.data }));
  } catch (error) {
    const errorMessage =
      error instanceof AxiosError
        ? error.response?.data.message || 'Something went wrong'
        : 'Something went wrong';
    dispatch(getLectorAsyncAction.failure({ error: errorMessage }));
  }
};
