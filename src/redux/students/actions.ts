import { createAction, createAsyncAction } from 'typesafe-actions';
import { Student, StudentWithCourses } from '../../services/studentsTypes';

export enum StudentsActions {
  GET_ALL_STUDENTS_REQUEST = '@students/GET_ALL_STUDENTS_REQUEST',
  GET_ALL_STUDENTS_SUCCESS = '@students/GET_ALL_STUDENTS_SUCCESS',
  GET_ALL_STUDENTS_FAILURE = '@students/GET_ALL_STUDENTS_FAILURE',

  GET_STUDENT_REQUEST = '@students/GET_STUDENT_REQUEST',
  GET_STUDENT_SUCCESS = '@students/GET_STUDENT_SUCCESS',
  GET_STUDENT_FAILURE = '@students/GET_STUDENT_FAILURE',

  ADD_STUDENT_REQUEST = '@students/ADD_STUDENT_REQUEST',
  ADD_STUDENT_SUCCESS = '@students/ADD_STUDENT_SUCCESS',
  ADD_STUDENT_FAILURE = '@students/GET_STUDENT_FAILURE',

  FILTER_STUDENTS = '@students/FILTER_STUDENTS',
}

export const getAllStudentsAsyncAction = createAsyncAction(
  StudentsActions.GET_ALL_STUDENTS_REQUEST,
  StudentsActions.GET_ALL_STUDENTS_SUCCESS,
  StudentsActions.GET_ALL_STUDENTS_FAILURE,
)<undefined, { students: Student[] }, { error: string }>();

export const getStudentAsyncAction = createAsyncAction(
  StudentsActions.GET_STUDENT_REQUEST,
  StudentsActions.GET_STUDENT_SUCCESS,
  StudentsActions.GET_STUDENT_FAILURE,
)<
  { id: string },
  { student: StudentWithCourses; id: string },
  { error: string; id: string }
>();

export const filterStudents = createAction(
  StudentsActions.FILTER_STUDENTS,
  ({
    sort,
    order,
    query,
    search,
  }: {
    sort: string;
    order: string;
    query: string;
    search: string;
  }) => ({
    sort,
    order,
    query,
    search,
  }),
)();
