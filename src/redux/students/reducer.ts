import { getType } from 'typesafe-actions';
import { GlobalAppActions } from '../actions';
import { RequestState } from '../../services/commonTypes';
import {
  filterStudents,
  getAllStudentsAsyncAction,
  getStudentAsyncAction,
} from './actions';
import { Student, StudentWithCourses } from '../../services/studentsTypes';

export interface StudentsState {
  students: Student[];
  studentsRequestState: RequestState;
  studentsError: string | null;
  student: {
    [id: number]: {
      requestState: RequestState;
      info: StudentWithCourses | null;
      error: string | null;
    };
  };
  filter: { sort: string; order: string; query: string; search: string };
}

const initialState: StudentsState = {
  students: [],
  studentsRequestState: RequestState.Unset,
  studentsError: null,
  student: {},
  filter: { sort: '', order: '', query: '', search: 'surname' },
};

export const reducer = (
  state = initialState,
  action: GlobalAppActions,
): StudentsState => {
  switch (action.type) {
    case getType(getAllStudentsAsyncAction.request): {
      return {
        ...state,
        studentsRequestState: RequestState.Waiting,
        studentsError: null,
      };
    }

    case getType(getAllStudentsAsyncAction.success): {
      return {
        ...state,
        students: action.payload.students,
        studentsRequestState: RequestState.Success,
        studentsError: null,
      };
    }

    case getType(getAllStudentsAsyncAction.failure): {
      return {
        ...state,
        studentsRequestState: RequestState.Failure,
        studentsError: action.payload.error,
      };
    }

    case getType(getStudentAsyncAction.request): {
      return {
        ...state,
        student: {
          ...state.student,
          [action.payload.id]: {
            info: null,
            error: null,
            requestState: RequestState.Waiting,
          },
        },
      };
    }

    case getType(getStudentAsyncAction.success): {
      return {
        ...state,
        student: {
          ...state.student,
          [action.payload.id]: {
            info: action.payload.student,
            error: null,
            requestState: RequestState.Success,
          },
        },
      };
    }

    case getType(getStudentAsyncAction.failure): {
      return {
        ...state,
        student: {
          ...state.student,
          [action.payload.id]: {
            info: null,
            error: action.payload.error,
            requestState: RequestState.Failure,
          },
        },
      };
    }

    case getType(filterStudents):
      return {
        ...state,
        filter: action.payload,
      };

    default:
      return state;
  }
};
