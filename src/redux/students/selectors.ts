import { RequestState } from '../../services/commonTypes';
import { Student, StudentWithCourses } from '../../services/studentsTypes';
import { GlobalAppState } from '../rootReducer';

export const selectStudents = (state: GlobalAppState): Student[] =>
  state.students.students;

export const selectAreStudentsLoading = (state: GlobalAppState): boolean =>
  state.students.studentsRequestState === RequestState.Waiting;

export const selectStudentsError = (state: GlobalAppState): string | null =>
  state.students.studentsError;

export const selectStudent = (
  state: GlobalAppState,
): {
  [id: string]: {
    requestState: RequestState;
    info: StudentWithCourses | null;
    error: string | null;
  };
} => state.students.student;

export const selectFilter = (state: GlobalAppState) => state.students.filter;
