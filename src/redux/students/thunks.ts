import { AppDispatch } from '../index';
import { ThunkAction } from 'redux-thunk';
import { GlobalAppState } from '../rootReducer';
import { GlobalAppActions } from '../actions';
import { AxiosError } from 'axios';
import { getAllStudentsApi, getStudentApi } from '../../api/students.api';
import { getAllStudentsAsyncAction, getStudentAsyncAction } from './actions';

export type ThunkAppType = ThunkAction<
  Promise<void>,
  GlobalAppState,
  undefined,
  GlobalAppActions
>;

export const getAllStudentsThunk =
  (sort: string, order: string, name: string, search: string) =>
  async (dispatch: AppDispatch) => {
    dispatch(getAllStudentsAsyncAction.request());
    try {
      const students = await getAllStudentsApi(sort, order, name, search);
      dispatch(getAllStudentsAsyncAction.success({ students: students.data }));
    } catch (error) {
      const errorMessage =
        error instanceof AxiosError
          ? error.response?.data.message || 'Something went wrong'
          : 'Something went wrong';
      dispatch(getAllStudentsAsyncAction.failure({ error: errorMessage }));
    }
  };

export const getStudentThunk =
  (id: string) => async (dispatch: AppDispatch) => {
    dispatch(getStudentAsyncAction.request({ id }));
    try {
      const student = await getStudentApi(id);
      dispatch(getStudentAsyncAction.success({ student: student.data, id }));
    } catch (error) {
      const errorMessage =
        error instanceof AxiosError
          ? error.response?.data.message || 'Something went wrong'
          : 'Something went wrong';
      dispatch(getStudentAsyncAction.failure({ error: errorMessage, id }));
    }
  };
