import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Route, Routes } from 'react-router-dom';
import { getUserThunk } from './redux/user/thunks';
import { stopUserLoadingAction } from './redux/user/actions';
import { LoginPage } from './containers/LoginPage/LoginPage';
import { ForgotPasswordPage } from './containers/ForgotPasswordPage/ForgotPasswordPage';
import { ResetPasswordPage } from './containers/ResetPasswordPage/ResetPasswordPage';
import { RegistrationPage } from './containers/RegistrationPage/RegistrationPage';
import { AuthRequire } from './components/AuthRequire/AuthRequire';
import { LectorsPage } from './containers/LectorsPage/LectorsPage';
import { CoursesPage } from './containers/CoursesPage/CoursesPage';
import { GroupsPage } from './containers/GroupsPage/GroupsPage';
import { StudentsPage } from './containers/StudentsPage/StudentsPage';
import { NotFoundPage } from './containers/NotFoundPage/NotFoundPage';
import { StudentPage } from './containers/StudentPage/StudentPage';

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (!token) {
      dispatch(stopUserLoadingAction());
    } else {
      dispatch(getUserThunk());
    }
  }, [dispatch]);

  return (
    <Routes>
      <Route path="/" element={<LoginPage />} />
      <Route path="/forgot-password" element={<ForgotPasswordPage />} />
      <Route path="/reset-password" element={<ResetPasswordPage />} />
      <Route path="/sign-up" element={<RegistrationPage />} />

      <Route
        path="/*"
        element={
          <AuthRequire>
            <Routes>
              <Route path="/courses" element={<CoursesPage />} />
              <Route path="/lectors" element={<LectorsPage />} />
              <Route path="/groups" element={<GroupsPage />} />
              <Route path="/students" element={<StudentsPage />} />
              <Route path="/students/:id" element={<StudentPage />} />
            </Routes>
          </AuthRequire>
        }
      />

      <Route path="*" element={<NotFoundPage />} />
    </Routes>
  );
}

export default App;
