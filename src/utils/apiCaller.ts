import axios, {
  AxiosRequestConfig,
  AxiosResponse,
  RawAxiosRequestHeaders,
} from 'axios';
import { getAccessToken } from './getAccessToken';

export const apiCaller = async (
  opts: AxiosRequestConfig = {},
): Promise<AxiosResponse> => {
  const { url = null, method = 'GET', params = {}, data = null } = opts;

  const headers: RawAxiosRequestHeaders = {};

  if (data instanceof FormData) {
    headers['Content-Type'] = 'multipart/form-data';
  } else {
    headers['Content-Type'] = 'application/json';
  }

  const token = getAccessToken();

  if (token) {
    headers['Authorization'] = `${token}`;
  }

  const requestURL = `https://university-back-av0y.onrender.com/api/v1/${url}`;

  const config = {
    url: requestURL,
    method,
    data,
    headers,
    params,
  };

  const axiosApiInstance = axios.create(config);

  return axiosApiInstance.request(config);
};
