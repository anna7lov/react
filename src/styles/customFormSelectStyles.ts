import { StylesConfig } from 'react-select';

export const customFormSelectStyles: StylesConfig = {
  control: (base, state) => ({
    ...base,
    '&:hover': { borderColor: 'none' },
    boxShadow: 'none',
    height: '48px',
    borderRadius: 'none',
    border: state.hasValue ? '1px solid #505e68' : '1px solid #cad2d6',
  }),
  placeholder: (base) => ({
    ...base,
    fontFamily: 'Lexend',
    fontSize: '15px',
    fontWeight: 400,
    lineHeight: '24px',
    color: '#909fa9',
  }),
  valueContainer: (base) => ({
    ...base,
    fontFamily: 'Lexend',
    fontSize: '15px',
    fontWeight: 400,
    paddingLeft: '14px',
  }),
  menu: (base) => ({
    ...base,
    margin: '0',
    boxShadow: 'none',
    border: '1px solid #cad2d6',
    borderTop: '1px solid #ffffff',
    borderRadius: 'none',
  }),
  option: (base, state) => ({
    ...base,
    height: '24px',
    padding: '0 12px',
    fontFamily: 'Nunito',
    fontSize: '14px',
    fontStyle: 'normal',
    fontWeight: 500,
    lineHeight: '24px',
    opacity: state.isSelected ? 1 : 0.45,
    color: '#3B4360',
    backgroundColor: 'none',
    cursor: 'pointer',
    '&:not(:last-child)': { marginBottom: '8px' },
    '&:hover': {
      color: state.isSelected ? '#3B4360' : '#3333ff',
      opacity: 1,
    },
  }),
  menuList: (base) => ({
    ...base,
    padding: '10px 0 5px',
  }),
};
