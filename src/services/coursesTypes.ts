export interface Course {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  name: string;
  description: string;
  hours: string;
  studentsCount: string;
}
