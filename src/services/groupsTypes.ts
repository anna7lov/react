export interface Group {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  name: string;
}
