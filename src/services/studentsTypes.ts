import { Course } from './coursesTypes';

export interface Student {
  id: number;
  name: string;
  surname: string;
  email: string;
  age: string;
  imagePath: string | null;
  groupName: number | null;
}

export interface StudentWithCourses extends Omit<Student, 'groupName'> {
  groupId: number | null;
  courses: Course[];
}
