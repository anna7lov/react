export interface Lector {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  name: string;
  surname: string;
  email: string;
}
