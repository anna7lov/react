import cn from 'classnames';
import { Lector } from '../../services/lectorsTypes';
import { Student } from '../../services/studentsTypes';
import { Group } from '../../services/groupsTypes';
import { Course } from '../../services/coursesTypes';
import styles from './Row.module.scss';
import sharedStyles from '../../styles/shared.module.scss';

interface RowProps {
  item: Lector | Student | Group | Course;
  columns: string[];
  onEditClick: (lectorId: number) => void;
}

export const Row = ({ item, columns, onEditClick }: RowProps) => {
  return (
    <div className={styles.row}>
      {columns.map((columnName) =>
        columnName === 'imagePath' && 'imagePath' in item ? (
          <div
            className={cn(styles.row__cell, sharedStyles.smallColumn)}
            key={columnName}
          >
            <img
              src={
                item.imagePath
                  ? `data:image/jpeg;base64,${item.imagePath}`
                  : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMgUT8jlhEYuFZp2zsJq8YOAzC3ISYJue6tg&usqp=CAU'
              }
              className={styles.row__image}
              alt=""
            />
          </div>
        ) : (
          <div
            className={cn(styles.row__cell, {
              [sharedStyles.oneColumn]: columns.length === 1,
              [sharedStyles.threeColumns]: columns.length === 3,
              [sharedStyles.fourColumns]: columns.length === 4,
              [sharedStyles.fiveColumns]:
                columns.length === 5 ||
                (columns.length === 6 && 'imagePath' in item),
            })}
            key={columnName}
          >
            {item[columnName as keyof (Lector | Student | Group | Course)] ||
              'None'}
          </div>
        ),
      )}
      <button
        onClick={() => onEditClick(item.id)}
        className={styles.row__button}
      >
        <svg
          width="19"
          height="19"
          viewBox="0 0 19 19"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M13.7574 0.996658L6.29145 8.4626L6.29886 12.7097L10.537 12.7023L18 5.2393V17.9967C18 18.5489 17.5523 18.9967 17 18.9967H1C0.44772 18.9967 0 18.5489 0 17.9967V1.99666C0 1.44438 0.44772 0.996658 1 0.996658H13.7574ZM17.4853 0.097168L18.8995 1.51138L9.7071 10.7038L8.2954 10.7062L8.2929 9.2896L17.4853 0.097168Z"
            fill="#BBB0C8"
          />
        </svg>
      </button>
    </div>
  );
};
