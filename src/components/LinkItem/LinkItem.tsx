import { Link } from 'react-router-dom';
import cn from 'classnames';
import styles from './LinkItem.module.scss';

type LinkItemVariants = 'filled' | 'text';

interface LinkProps {
  address: string;
  title: string;
  variant: LinkItemVariants;
}

export const LinkItem = ({
  address,
  title,
  variant,
}: LinkProps): JSX.Element => {
  return (
    <Link
      to={address}
      className={cn(styles.link, {
        [styles.filled]: variant === 'filled',
        [styles.text]: variant === 'text',
      })}
    >
      {title}
    </Link>
  );
};
