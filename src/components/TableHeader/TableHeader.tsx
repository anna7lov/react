import cn from 'classnames';
import styles from './TableHeader.module.scss';
import sharedStyles from '../../styles/shared.module.scss';

interface TableHeaderProps {
  titles: string[];
}

export const TableHeader = ({ titles }: TableHeaderProps) => {
  return (
    <div className={styles.tableHeader}>
      {titles.map((title) => (
        <div
          className={cn(styles.tableHeader__title, {
            [sharedStyles.oneColumn]: titles.length === 1,
            [sharedStyles.smallColumn]: title === '',
            [sharedStyles.threeColumns]: titles.length === 3,
            [sharedStyles.fourColumns]: titles.length === 4,
            [sharedStyles.fiveColumns]:
              titles.length === 5 ||
              (titles.length === 6 && titles.includes('') && title !== ''),
          })}
          key={title}
        >
          {title}
        </div>
      ))}
    </div>
  );
};
