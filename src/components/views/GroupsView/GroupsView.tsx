import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  selectAreGroupsLoading,
  selectGroupError,
  selectGroups,
} from '../../../redux/groups/selectors';
import { getAllGroupsThunk } from '../../../redux/groups/thunks';
import { Row } from '../../Row/Row';
import { TableHeader } from '../../TableHeader/TableHeader';
import { AddButton } from '../../AddButton/AddButton';
import { Modal } from '../../Modal/Modal';
import { UpdateGroupForm } from '../../forms/GroupForm/UpdateGroupForm';
import { AddGroupForm } from '../../forms/GroupForm/AddGroupForm';
import { Error } from '../../Error/Error';
import { Loading } from '../../Loading/Loading';
import { titles, columns } from './constants';
import styles from '../../../styles/shared.module.scss';

export const GroupsView = (): JSX.Element => {
  const dispatch = useDispatch();
  const groups = useSelector(selectGroups);
  const areGroupsLoading = useSelector(selectAreGroupsLoading);
  const groupsError = useSelector(selectGroupError);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [selectedGroupId, setSelectedGroupId] = useState<number | null>(null);

  useEffect(() => {
    dispatch(getAllGroupsThunk());
  }, [dispatch]);

  const onAddClicked = () => {
    setIsModalVisible(true);
  };

  const onCloseClicked = () => {
    setIsModalVisible(false);
  };

  const onEditClick = (lectorId: number) => {
    setSelectedGroupId(lectorId);
    setIsEditModalVisible(true);
  };

  const onEditModalClose = () => {
    setSelectedGroupId(null);
    setIsEditModalVisible(false);
  };

  return (
    <>
      {areGroupsLoading ? (
        <Loading />
      ) : groupsError ? (
        <Error title={groupsError} />
      ) : (
        <div>
          <div className={styles.toolsWrapper}>
            <AddButton title="group" onAddClicked={onAddClicked} />
          </div>

          <div className={styles.list}>
            <TableHeader titles={titles} />
            {groups.map((group) => (
              <Row
                item={group}
                key={group.id}
                onEditClick={onEditClick}
                columns={columns}
              />
            ))}
          </div>
          {isModalVisible && (
            <Modal title="Add new group" onCloseClicked={onCloseClicked}>
              <AddGroupForm />
            </Modal>
          )}

          {isEditModalVisible && selectedGroupId && (
            <Modal title="Edit Group" onCloseClicked={onEditModalClose}>
              <UpdateGroupForm groupId={selectedGroupId} />
            </Modal>
          )}
        </div>
      )}
    </>
  );
};
