import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  selectAreCoursesLoading,
  selectCourseError,
  selectCourses,
} from '../../../redux/courses/selectors';
import { getAllCoursesThunk } from '../../../redux/courses/thunks';
import { Row } from '../../Row/Row';
import { TableHeader } from '../../TableHeader/TableHeader';
import { AddButton } from '../../AddButton/AddButton';
import { Modal } from '../../Modal/Modal';
import { AddCourseForm } from '../../forms/CourseForm/AddCourseForm';
import { UpdateCourseForm } from '../../forms/CourseForm/UpdateCourseForm';
import { Loading } from '../../Loading/Loading';
import { Error } from '../../Error/Error';
import { titles, columns } from './constants';
import styles from '../../../styles/shared.module.scss';

export const CoursesView = (): JSX.Element => {
  const dispatch = useDispatch();
  const courses = useSelector(selectCourses);
  const areCoursesLoading = useSelector(selectAreCoursesLoading);
  const coursesError = useSelector(selectCourseError);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [selectedCourseId, setSelectedCourseId] = useState<number | null>(null);

  useEffect(() => {
    dispatch(getAllCoursesThunk());
  }, [dispatch]);

  const onAddClicked = () => {
    setIsModalVisible(true);
  };

  const onCloseClicked = () => {
    setIsModalVisible(false);
  };

  const onEditClick = (lectorId: number) => {
    setSelectedCourseId(lectorId);
    setIsEditModalVisible(true);
  };

  const onEditModalClose = () => {
    setSelectedCourseId(null);
    setIsEditModalVisible(false);
  };

  return (
    <>
      {areCoursesLoading ? (
        <Loading />
      ) : coursesError ? (
        <Error title={coursesError} />
      ) : (
        <div>
          <div className={styles.toolsWrapper}>
            <AddButton title="course" onAddClicked={onAddClicked} />
          </div>

          <div className={styles.list}>
            <TableHeader titles={titles} />
            {courses.map((course) => (
              <Row
                item={course}
                key={course.id}
                onEditClick={onEditClick}
                columns={columns}
              />
            ))}
          </div>
          {isModalVisible && (
            <Modal title="Add new course" onCloseClicked={onCloseClicked}>
              <AddCourseForm />
            </Modal>
          )}

          {isEditModalVisible && selectedCourseId && (
            <Modal title="Edit Course" onCloseClicked={onEditModalClose}>
              <UpdateCourseForm courseId={selectedCourseId} />
            </Modal>
          )}
        </div>
      )}
    </>
  );
};
