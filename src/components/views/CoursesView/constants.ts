export const columns = ['name', 'description', 'hours', 'studentsCount'];

export const titles = ['name', 'description', 'hours', 'students'];
