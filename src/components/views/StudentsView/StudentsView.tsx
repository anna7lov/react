import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { filterStudents } from '../../../redux/students/actions';
import {
  selectStudents,
  selectAreStudentsLoading,
  selectStudentsError,
  selectFilter,
} from '../../../redux/students/selectors';
import { getAllStudentsThunk } from '../../../redux/students/thunks';
import { AddButton } from '../../AddButton/AddButton';
import { Loading } from '../../Loading/Loading';
import { Modal } from '../../Modal/Modal';
import { Row } from '../../Row/Row';
import { TableHeader } from '../../TableHeader/TableHeader';
import { Tool } from '../../Tool/Tool';
import { AddStudentForm } from '../../forms/AddStudentForm/AddStudentForm';
import { Error } from '../../../components/Error/Error';
import { sortOptions, searchOptions, titles, columns } from './constants';
import styles from '../../../styles/shared.module.scss';

export const StudentsView = (): JSX.Element => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const students = useSelector(selectStudents);
  const areStudentsLoading = useSelector(selectAreStudentsLoading);
  const studentsError = useSelector(selectStudentsError);
  const filter = useSelector(selectFilter);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const onQueryChange = (newQuery: string) => {
    dispatch(
      filterStudents({
        ...filter,
        query: newQuery,
      }),
    );
  };

  const onSortChange = (newSort: string, newOrder: string) => {
    dispatch(filterStudents({ ...filter, sort: newSort, order: newOrder }));
  };

  const onSearchChange = (newSearch: string) => {
    dispatch(filterStudents({ ...filter, search: newSearch }));
  };

  const onEditClick = (studentId: number) => {
    navigate(`/students/${studentId}`);
  };

  useEffect(() => {
    dispatch(
      getAllStudentsThunk(
        filter.sort,
        filter.order,
        filter.query,
        filter.search,
      ),
    );
  }, [dispatch, filter]);

  const onAddClicked = () => {
    setIsModalVisible(true);
  };

  const onCloseClicked = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      {areStudentsLoading ? (
        <Loading />
      ) : studentsError ? (
        <Error title={studentsError} />
      ) : (
        <div>
          <div className={styles.toolsWrapper}>
            <Tool
              sortOptions={sortOptions}
              searchOptions={searchOptions}
              onNameChange={onQueryChange}
              onSortChange={onSortChange}
              onSearchChange={onSearchChange}
              filter={filter}
            />
            <AddButton title="student" onAddClicked={onAddClicked} />
          </div>
          <div>
            <TableHeader titles={titles} />
            {students.map((student) => (
              <div key={student.id}>
                <Row
                  item={student}
                  key={student.id}
                  onEditClick={onEditClick}
                  columns={columns}
                />
              </div>
            ))}
          </div>
          {isModalVisible ? (
            <Modal title="Add new student" onCloseClicked={onCloseClicked}>
              <AddStudentForm />
            </Modal>
          ) : null}
        </div>
      )}
    </>
  );
};
