export const sortOptions = [
  { value: 'surname-asc', label: 'A-Z' },
  { value: 'surname-desc', label: 'Z-A' },
  { value: 'createdAt-desc', label: 'Newest first' },
  { value: 'createdAt-asc', label: 'Latest first' },
  { value: '-', label: 'All' },
];

export const searchOptions = [
  { value: 'name', label: 'Name' },
  { value: 'surname', label: 'Surname' },
  { value: 'groupName', label: 'Group' },
];

export const columns = [
  'imagePath',
  'name',
  'surname',
  'email',
  'age',
  'groupName',
];

export const titles = ['', 'name', 'surname', 'email', 'age', 'group'];
