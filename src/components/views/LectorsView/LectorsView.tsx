import { useDispatch, useSelector } from 'react-redux';
import ReactPaginate from 'react-paginate';
import { getAllLectorsThunk } from '../../../redux/lectors/thunks';
import { useEffect, useState } from 'react';
import {
  selectAreLectorsLoading,
  selectLectors,
  selectLectorsCount,
  selectLectorsError,
} from '../../../redux/lectors/selectors';
import { Loading } from '../../Loading/Loading';
import { Row } from '../../Row/Row';
import { Modal } from '../../Modal/Modal';
import { UpdateLectorForm } from '../../forms/UpdateLectorForm/UpdateLectorForm';
import { TableHeader } from '../../TableHeader/TableHeader';
import { AddButton } from '../../AddButton/AddButton';
import { Error } from '../../Error/Error';
import { AddLectorForm } from '../../forms/AddLectorForm/AddLectorForm';
import { paginateLectors } from '../../../redux/lectors/actions';
import { titles, columns } from './constants';
import sharedStyles from '../../../styles/shared.module.scss';
import styles from './LectorsView.module.scss';

export const LectorsView = (): JSX.Element => {
  const dispatch = useDispatch();
  const lectors = useSelector(selectLectors);
  const areLectorsLoading = useSelector(selectAreLectorsLoading);
  const lectorsError = useSelector(selectLectorsError);
  const lectorsCount = useSelector(selectLectorsCount);
  const pageCount = Math.ceil(lectorsCount / 5);
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedLectorId, setSelectedLectorId] = useState<number | null>(null);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);

  useEffect(() => {
    dispatch(getAllLectorsThunk(currentPage));
  }, [dispatch, currentPage]);

  const handlePageClick = ({ selected }: { selected: number }) => {
    setCurrentPage(selected + 1);
    dispatch(paginateLectors({ page: selected + 1 }));
  };

  const onAddClicked = () => {
    setIsModalVisible(true);
  };

  const onCloseClicked = () => {
    setIsModalVisible(false);
  };

  const onEditClick = (lectorId: number) => {
    setSelectedLectorId(lectorId);
    setIsEditModalVisible(true);
  };

  const onEditModalClose = () => {
    setSelectedLectorId(null);
    setIsEditModalVisible(false);
  };

  return (
    <>
      {areLectorsLoading ? (
        <Loading />
      ) : lectorsError ? (
        <Error title={lectorsError} />
      ) : (
        <div>
          <div className={sharedStyles.toolsWrapper}>
            <AddButton title="lector" onAddClicked={onAddClicked} />
          </div>
          <div className={sharedStyles.list}>
            <TableHeader titles={titles} />
            {lectors.map((lector) => (
              <Row
                item={lector}
                key={lector.id}
                onEditClick={onEditClick}
                columns={columns}
              />
            ))}
          </div>
          <ReactPaginate
            previousLabel={'Previous'}
            nextLabel={'Next'}
            breakLabel={'...'}
            pageCount={pageCount}
            marginPagesDisplayed={2}
            pageRangeDisplayed={3}
            onPageChange={handlePageClick}
            forcePage={currentPage - 1}
            className={styles.paginate}
            activeClassName={styles.active}
            breakClassName={styles.break}
            breakLinkClassName={styles.pageLink}
            disabledClassName={styles.disabled}
            previousClassName={styles.previous}
            nextClassName={styles.next}
            pageLinkClassName={styles.pageLink}
            previousLinkClassName={styles.pageLink}
            nextLinkClassName={styles.pageLink}
            pageClassName={styles.item}
          />

          {isModalVisible && (
            <Modal title="Add new lector" onCloseClicked={onCloseClicked}>
              <AddLectorForm />
            </Modal>
          )}

          {isEditModalVisible && selectedLectorId && (
            <Modal title="Edit Lector" onCloseClicked={onEditModalClose}>
              <UpdateLectorForm
                lectorId={selectedLectorId}
                onClose={onEditModalClose}
              />
            </Modal>
          )}
        </div>
      )}
    </>
  );
};
