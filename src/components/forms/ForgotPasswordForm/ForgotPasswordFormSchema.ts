import * as Yup from 'yup';
import { ForgotPasswordFormValues } from './ForgotPasswordForm';

export const forgotPasswordFormSchema: Yup.Schema<ForgotPasswordFormValues> =
  Yup.object().shape({
    forgotPasswordEmail: Yup.string()
      .email('Please enter a valid email')
      .required('Required'),
  });
