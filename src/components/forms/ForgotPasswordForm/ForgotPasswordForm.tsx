import { ToastContainer, toast } from 'react-toastify';
import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';
import {
  selectIsForgotPasswordFailed,
  selectIsForgotPasswordLoading,
  selectIsForgotPasswordSuccess,
} from '../../../redux/user/selectors';
import { forgotPasswordThunk } from '../../../redux/user/thunks';
import { Button } from '../../Button/Button';
import { InputField } from '../../InputField/InputField';
import { LinkItem } from '../../LinkItem/LinkItem';
import { AuthLayout } from '../../layouts/auth/AuthLayout/AuthLayout';
import { forgotPasswordFormSchema } from './ForgotPasswordFormSchema';
import 'react-toastify/dist/ReactToastify.css';
import styles from '../../../styles/shared.module.scss';

export interface ForgotPasswordFormValues {
  forgotPasswordEmail: string;
}

export const ForgotPasswordForm = (): JSX.Element => {
  const dispatch = useDispatch();
  const forgotPasswordError = useSelector(selectIsForgotPasswordFailed);
  const isForgotPasswordLoading = useSelector(selectIsForgotPasswordLoading);
  const isForgotPasswordSuccess = useSelector(selectIsForgotPasswordSuccess);

  const onForgotPasswordFormSubmit = async (
    values: ForgotPasswordFormValues,
  ): Promise<void> => {
    dispatch(
      forgotPasswordThunk({
        email: values.forgotPasswordEmail,
      }),
    );
  };

  const { values, errors, touched, handleBlur, handleChange, handleSubmit } =
    useFormik<ForgotPasswordFormValues>({
      initialValues: {
        forgotPasswordEmail: '',
      },
      validationSchema: forgotPasswordFormSchema,
      onSubmit: onForgotPasswordFormSubmit,
    });

  if (isForgotPasswordSuccess) {
    toast.success('Password recovery email sent successfully', {
      position: toast.POSITION.TOP_CENTER,
      autoClose: false,
    });
  }

  if (forgotPasswordError) {
    toast.error(forgotPasswordError, {
      position: toast.POSITION.TOP_CENTER,
      autoClose: false,
    });
  }

  return (
    <AuthLayout
      title="Forgot Password"
      paragraph="Don't worry, happens to the best of us.
      Enter the email address associated with your account and we'll send you a link to reset."
    >
      <ToastContainer />
      <form noValidate onSubmit={handleSubmit} className={styles.formContainer}>
        <InputField
          label="Email"
          type="email"
          id="forgot-password-email"
          name="forgotPasswordEmail"
          placeholder="name@mail.com"
          value={values.forgotPasswordEmail}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.forgotPasswordEmail}
          touched={touched.forgotPasswordEmail}
        />
        <Button
          isDisabled={isForgotPasswordLoading || isForgotPasswordSuccess}
          title={'Reset'}
        />
      </form>
      <LinkItem address="/" title="Cancel" variant="text" />
    </AuthLayout>
  );
};
