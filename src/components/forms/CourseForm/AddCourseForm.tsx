import { useFormik } from 'formik';
import { Button } from '../../Button/Button';
import { InputField } from '../../InputField/InputField';
import { AxiosError } from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import { CourseFormValues, courseFormSchema } from './CourseFormSchema';
import { addCourseApi } from '../../../api/courses.api';

export const AddCourseForm = (): JSX.Element => {
  const onAddCourseFormSubmit = async (
    values: CourseFormValues,
  ): Promise<void> => {
    try {
      await addCourseApi({
        name: values.courseName,
        description: values.courseDescription,
        hours: +values.courseHours,
      });
      window.location.reload();
    } catch (error) {
      const errorMessage =
        error instanceof AxiosError
          ? error.response?.data.message || 'Something went wrong'
          : 'Something went wrong';

      toast.error(errorMessage, {
        position: toast.POSITION.TOP_CENTER,
        autoClose: false,
      });
    }
  };

  const {
    values,
    errors,
    touched,
    isSubmitting,
    handleBlur,
    handleChange,
    handleSubmit,
  } = useFormik<CourseFormValues>({
    initialValues: {
      courseName: '',
      courseDescription: '',
      courseHours: '',
    },
    validationSchema: courseFormSchema,
    onSubmit: onAddCourseFormSubmit,
  });

  return (
    <>
      <ToastContainer />
      <form noValidate onSubmit={handleSubmit}>
        <InputField
          label="Name"
          type="text"
          id="course-name"
          name="courseName"
          placeholder="Name"
          value={values.courseName}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.courseName}
          touched={touched.courseName}
        />
        <InputField
          label="Description"
          type="text"
          id="course-description"
          name="courseDescription"
          placeholder="Description"
          value={values.courseDescription}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.courseDescription}
          touched={touched.courseDescription}
        />
        <InputField
          label="Hours"
          type="text"
          id="course-hours"
          name="courseHours"
          placeholder="Hours"
          value={values.courseHours}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.courseHours}
          touched={touched.courseHours}
        />
        <Button title="Create" isDisabled={isSubmitting} />
      </form>
    </>
  );
};
