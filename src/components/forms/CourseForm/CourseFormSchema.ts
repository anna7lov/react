import * as Yup from 'yup';

export interface CourseFormValues {
  courseName: string;
  courseDescription: string;
  courseHours: string;
}

export const courseFormSchema = Yup.object().shape({
  courseName: Yup.string()
    .max(25, 'Max length 25 characters')
    .required('Required'),

  courseDescription: Yup.string()
    .max(25, 'Max length 25 characters')
    .required('Required'),

  courseHours: Yup.number().required('Required').min(1).max(1000),
});

export type AddStudentFormSchema = Yup.InferType<typeof courseFormSchema>;
