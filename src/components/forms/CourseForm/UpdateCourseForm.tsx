import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { useFormik } from 'formik';
import { Button } from '../../Button/Button';
import { InputField } from '../../InputField/InputField';
import { AxiosError } from 'axios';
import { Error } from '../../Error/Error';
import { Loading } from '../../Loading/Loading';
import { ToastContainer, toast } from 'react-toastify';
import { updateCourseByIdApi } from '../../../api/courses.api';
import { CourseFormValues, courseFormSchema } from './CourseFormSchema';
import {
  selectCourse,
  selectCourseError,
  selectIsCourseLoading,
} from '../../../redux/courses/selectors';
import { getCourseThunk } from '../../../redux/courses/thunks';

export interface GroupFormProps {
  courseId: number;
}

export const UpdateCourseForm = ({ courseId }: GroupFormProps): JSX.Element => {
  const dispatch = useDispatch();
  const course = useSelector(selectCourse);
  const isCourseLoading = useSelector(selectIsCourseLoading);
  const courseError = useSelector(selectCourseError);

  useEffect(() => {
    dispatch(getCourseThunk(courseId));
  }, [courseId]);

  useEffect(() => {
    if (course) {
      setValues({
        courseName: course.name,
        courseDescription: course.description,
        courseHours: course.hours,
      });
    }
  }, [course]);

  const onUpdateCourseFormSubmit = async (
    values: CourseFormValues,
  ): Promise<void> => {
    try {
      await updateCourseByIdApi({
        id: course.id,
        name: values.courseName,
        description: values.courseDescription,
        hours: values.courseHours,
      });
      window.location.reload();
    } catch (error) {
      const errorMessage =
        error instanceof AxiosError
          ? error.response?.data.message || 'Something went wrong'
          : 'Something went wrong';

      toast.error(errorMessage, {
        position: toast.POSITION.TOP_CENTER,
        autoClose: false,
      });
    }
  };

  const {
    values,
    errors,
    touched,
    isSubmitting,
    handleBlur,
    handleChange,
    handleSubmit,
    setValues,
  } = useFormik<CourseFormValues>({
    initialValues: {
      courseName: '',
      courseDescription: '',
      courseHours: '',
    },
    validationSchema: courseFormSchema,
    onSubmit: onUpdateCourseFormSubmit,
  });

  return (
    <>
      <ToastContainer />
      {isCourseLoading ? (
        <Loading />
      ) : courseError ? (
        <Error title={courseError} />
      ) : (
        <form noValidate onSubmit={handleSubmit}>
          <InputField
            label="Name"
            type="text"
            id="course-name"
            name="courseName"
            placeholder="Name"
            value={values.courseName}
            handleChange={handleChange}
            handleBlur={handleBlur}
            error={errors.courseName}
            touched={touched.courseName}
          />
          <InputField
            label="Description"
            type="text"
            id="course-description"
            name="courseDescription"
            placeholder="Description"
            value={values.courseDescription}
            handleChange={handleChange}
            handleBlur={handleBlur}
            error={errors.courseDescription}
            touched={touched.courseDescription}
          />
          <InputField
            label="Hours"
            type="text"
            id="course-hours"
            name="courseHours"
            placeholder="Hours"
            value={values.courseHours}
            handleChange={handleChange}
            handleBlur={handleBlur}
            error={errors.courseHours}
            touched={touched.courseHours}
          />
          <Button title="Update" isDisabled={isSubmitting} />
        </form>
      )}
    </>
  );
};
