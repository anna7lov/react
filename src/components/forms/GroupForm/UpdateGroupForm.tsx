import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { useFormik } from 'formik';
import { Button } from '../../Button/Button';
import { InputField } from '../../InputField/InputField';
import { AxiosError } from 'axios';
import { GroupFormValues, groupFormSchema } from './GroupFormSchema';
import { updateGroupByIdApi } from '../../../api/groups.api';
import { getGroupThunk } from '../../../redux/groups/thunks';
import {
  selectGroup,
  selectGroupError,
  selectIsGroupLoading,
} from '../../../redux/groups/selectors';
import { Error } from '../../Error/Error';
import { Loading } from '../../Loading/Loading';
import { ToastContainer, toast } from 'react-toastify';

export interface GroupFormProps {
  groupId: number;
}

export const UpdateGroupForm = ({ groupId }: GroupFormProps): JSX.Element => {
  const dispatch = useDispatch();
  const group = useSelector(selectGroup);
  const isGroupLoading = useSelector(selectIsGroupLoading);
  const groupError = useSelector(selectGroupError);

  useEffect(() => {
    dispatch(getGroupThunk(groupId));
  }, [groupId]);

  useEffect(() => {
    if (group) {
      setValues({
        groupName: group.name,
      });
    }
  }, [group]);

  const onUpdateGroupFormSubmit = async (
    values: GroupFormValues,
  ): Promise<void> => {
    try {
      await updateGroupByIdApi({
        id: group.id,
        name: values.groupName,
      });
      window.location.reload();
    } catch (error) {
      const errorMessage =
        error instanceof AxiosError
          ? error.response?.data.message || 'Something went wrong'
          : 'Something went wrong';

      toast.error(errorMessage, {
        position: toast.POSITION.TOP_CENTER,
        autoClose: false,
      });
    }
  };

  const {
    values,
    errors,
    touched,
    isSubmitting,
    handleBlur,
    handleChange,
    handleSubmit,
    setValues,
  } = useFormik<GroupFormValues>({
    initialValues: {
      groupName: '',
    },
    validationSchema: groupFormSchema,
    onSubmit: onUpdateGroupFormSubmit,
  });

  return (
    <>
      <ToastContainer />
      {isGroupLoading ? (
        <Loading />
      ) : groupError ? (
        <Error title={groupError} />
      ) : (
        <form noValidate onSubmit={handleSubmit}>
          <InputField
            label="Name"
            type="text"
            id="group-name"
            name="groupName"
            placeholder="Name"
            value={values.groupName}
            handleChange={handleChange}
            handleBlur={handleBlur}
            error={errors.groupName}
            touched={touched.groupName}
          />
          <Button title="Update" isDisabled={isSubmitting} />
        </form>
      )}
    </>
  );
};
