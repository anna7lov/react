import { useFormik } from 'formik';
import { AxiosError } from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import { Button } from '../../Button/Button';
import { InputField } from '../../InputField/InputField';
import { addGroupApi } from '../../../api/groups.api';
import { groupFormSchema } from './GroupFormSchema';

export interface GroupFormValues {
  groupName: string;
}

export const AddGroupForm = (): JSX.Element => {
  const onAddGroupFormSubmit = async (
    values: GroupFormValues,
  ): Promise<void> => {
    try {
      await addGroupApi({
        name: values.groupName,
      });
      window.location.reload();
    } catch (error) {
      const errorMessage =
        error instanceof AxiosError
          ? error.response?.data.message || 'Something went wrong'
          : 'Something went wrong';

      toast.error(errorMessage, {
        position: toast.POSITION.TOP_CENTER,
        autoClose: false,
      });
    }
  };

  const {
    values,
    errors,
    touched,
    isSubmitting,
    handleBlur,
    handleChange,
    handleSubmit,
  } = useFormik<GroupFormValues>({
    initialValues: {
      groupName: '',
    },
    validationSchema: groupFormSchema,
    onSubmit: onAddGroupFormSubmit,
  });

  return (
    <>
      <ToastContainer />
      <form noValidate onSubmit={handleSubmit}>
        <InputField
          label="Name"
          type="text"
          id="group-name"
          name="groupName"
          placeholder="Name"
          value={values.groupName}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.groupName}
          touched={touched.groupName}
        />
        <Button title="Create" isDisabled={isSubmitting} />
      </form>
    </>
  );
};
