import * as Yup from 'yup';

export interface GroupFormValues {
  groupName: string;
}

export const groupFormSchema: Yup.Schema<GroupFormValues> = Yup.object().shape({
  groupName: Yup.string()
    .max(25, 'Max length 25 characters')
    .required('Required'),
});
