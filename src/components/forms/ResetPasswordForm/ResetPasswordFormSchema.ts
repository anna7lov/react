import * as Yup from 'yup';
import { ResetPasswordFormValues } from './ResetPasswordForm';

export const resetPasswordFormSchema: Yup.Schema<ResetPasswordFormValues> =
  Yup.object().shape({
    newPassword: Yup.string()
      .min(6, 'Min length 6 characters')
      .required('Required'),

    confirmPassword: Yup.string()
      .oneOf([Yup.ref('newPassword'), undefined], 'Passwords must match')
      .required('Required'),
  });
