import { useState } from 'react';
import { useFormik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import { ToastContainer, toast } from 'react-toastify';
import { resetPasswordFormSchema } from './ResetPasswordFormSchema';
import { resetPasswordThunk } from '../../../redux/user/thunks';
import { selectIsResetPasswordFailed } from '../../../redux/user/selectors';
import { Button } from '../../Button/Button';
import { InputField } from '../../InputField/InputField';
import { AuthLayout } from '../../layouts/auth/AuthLayout/AuthLayout';
import { Checkbox } from '../../Checkbox/Checkbox';

export interface ResetPasswordFormValues {
  newPassword: string;
  confirmPassword: string;
}

export const ResetPasswordForm = (): JSX.Element => {
  const dispatch = useDispatch();
  const [showPassword, setShowPassword] = useState(false);
  const resetToken = new URLSearchParams(window.location.search).get('token');
  const resetPasswordError = useSelector(selectIsResetPasswordFailed);

  const handleCheckboxChange = () => {
    setShowPassword(!showPassword);
  };

  const onResetPasswordFormSubmit = async (
    values: ResetPasswordFormValues,
  ): Promise<void> => {
    dispatch(
      resetPasswordThunk({
        token: resetToken,
        password: values.newPassword,
        passwordConfirmation: values.confirmPassword,
      }),
    );
  };

  const { values, errors, touched, handleBlur, handleChange, handleSubmit } =
    useFormik<ResetPasswordFormValues>({
      initialValues: {
        newPassword: '',
        confirmPassword: '',
      },
      validationSchema: resetPasswordFormSchema,
      onSubmit: onResetPasswordFormSubmit,
    });

  if (resetPasswordError) {
    toast.error(resetPasswordError, {
      position: toast.POSITION.TOP_CENTER,
      autoClose: false,
    });
  }

  return (
    <AuthLayout title="Reset Your Password">
      <ToastContainer />
      <form noValidate onSubmit={handleSubmit}>
        <InputField
          label="New Password"
          type={showPassword ? 'text' : 'password'}
          id="new-password"
          name="newPassword"
          placeholder="******"
          value={values.newPassword}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.newPassword}
          touched={touched.newPassword}
        />
        <InputField
          label="Confirm Password"
          type={showPassword ? 'text' : 'password'}
          id="confirm-password"
          name="confirmPassword"
          placeholder="******"
          value={values.confirmPassword}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.confirmPassword}
          touched={touched.confirmPassword}
        />
        <Checkbox
          label="Show Password"
          id="reset-password-checkbox"
          isChecked={showPassword}
          handleChange={handleCheckboxChange}
        />
        <Button title="Reset" />
      </form>
    </AuthLayout>
  );
};
