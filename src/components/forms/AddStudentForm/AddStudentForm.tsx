import Select, { DropdownIndicatorProps, components } from 'react-select';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';
import { AxiosError } from 'axios';
import { selectCourses } from '../../../redux/courses/selectors';
import { getAllCoursesThunk } from '../../../redux/courses/thunks';
import { selectGroups } from '../../../redux/groups/selectors';
import { getAllGroupsThunk } from '../../../redux/groups/thunks';
import { getAllStudentsThunk } from '../../../redux/students/thunks';
import { addStudentApi } from '../../../api/students.api';
import { selectFilter } from '../../../redux/students/selectors';
import { InputField } from '../../InputField/InputField';
import { Button } from '../../Button/Button';
import { addStudentFormSchema } from './AddStudentFormSchema';
import { customFormSelectStyles } from '../../../styles/customFormSelectStyles';
import { ToastContainer, toast } from 'react-toastify';
import styles from './AddStudentForm.module.scss';

export interface AddStudentFormValues {
  studentName: string;
  studentSurname: string;
  studentEmail: string;
  studentAge: string;
  studentCourse: number[];
  studentGroup: number | null;
}
const DropdownIndicator = (props: DropdownIndicatorProps) => {
  return (
    <components.DropdownIndicator {...props}>
      <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path d="M12 14.5L17 9.5H7L12 14.5Z" fill="black" />
      </svg>
    </components.DropdownIndicator>
  );
};

export const AddStudentForm = (): JSX.Element => {
  const dispatch = useDispatch();
  const groups = useSelector(selectGroups);
  const courses = useSelector(selectCourses);
  const filter = useSelector(selectFilter);

  const groupOptions = groups.map((group) => ({
    value: group.id,
    label: group.name,
  }));
  const courseOptions = courses.map((course) => ({
    value: course.id,
    label: course.name,
  }));

  useEffect(() => {
    dispatch(getAllGroupsThunk());
    dispatch(getAllCoursesThunk());
  }, [dispatch]);

  const onAddStudentFormSubmit = async (
    values: AddStudentFormValues,
  ): Promise<void> => {
    try {
      const coursesList = values.studentCourse.map((course: any) => course.id);
      await addStudentApi({
        name: values.studentName,
        surname: values.studentSurname,
        email: values.studentEmail,
        age: +values.studentAge,
        coursesList: coursesList,
        groupId: values.studentGroup,
      });

      dispatch(
        getAllStudentsThunk(
          filter.sort,
          filter.order,
          filter.query,
          filter.search,
        ),
      );
    } catch (error) {
      const errorMessage =
        error instanceof AxiosError
          ? error.response?.data.message || 'Something went wrong'
          : 'Something went wrong';

      toast.error(errorMessage, {
        position: toast.POSITION.TOP_CENTER,
        autoClose: false,
      });
      console.log(errorMessage);
    }
  };

  const {
    values,
    errors,
    touched,
    isSubmitting,
    handleBlur,
    handleChange,
    handleSubmit,
    setValues,
  } = useFormik<AddStudentFormValues>({
    initialValues: {
      studentName: '',
      studentSurname: '',
      studentEmail: '',
      studentAge: '',
      studentCourse: [],
      studentGroup: null,
    },
    validationSchema: addStudentFormSchema,
    onSubmit: onAddStudentFormSubmit,
  });

  const handleGroupChange = (selectedOption: any) => {
    setValues({
      ...values,
      studentGroup: selectedOption?.value,
    });
  };

  const handleCoursesChange = (selectedOptions: any) => {
    setValues({
      ...values,
      studentCourse: selectedOptions.map((option: any) => ({
        id: option.value,
        label: option.label,
      })),
    });
  };

  return (
    <>
      <ToastContainer />
      <form noValidate onSubmit={handleSubmit}>
        <InputField
          label="Name"
          type="text"
          id="registration-name"
          name="studentName"
          placeholder="Name"
          value={values.studentName}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.studentName}
          touched={touched.studentName}
        />
        <InputField
          label="Surname"
          type="text"
          id="registration-surname"
          name="studentSurname"
          placeholder="Surname"
          value={values.studentSurname}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.studentSurname}
          touched={touched.studentSurname}
        />
        <InputField
          label="Email"
          type="email"
          id="registration-email"
          name="studentEmail"
          placeholder="name@mail.com"
          value={values.studentEmail}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.studentEmail}
          touched={touched.studentEmail}
        />
        <InputField
          label="Age"
          type="text"
          id="registration-email"
          name="studentAge"
          placeholder="Age"
          value={values.studentAge}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.studentAge}
          touched={touched.studentAge}
        />
        <label htmlFor="group-select" className={styles.select__label}>
          Group
        </label>
        <Select
          id="group-select"
          styles={customFormSelectStyles}
          options={groupOptions}
          className={styles.select}
          components={{ DropdownIndicator, IndicatorSeparator: null }}
          value={groupOptions.find(
            (option) => option.value === values.studentGroup,
          )}
          onChange={handleGroupChange}
        />
        <label htmlFor="course-select" className={styles.select__label}>
          Courses
        </label>
        <Select
          isMulti={true}
          id="course-select"
          styles={customFormSelectStyles}
          options={courseOptions}
          className={styles.select}
          components={{ DropdownIndicator, IndicatorSeparator: null }}
          value={courseOptions.filter((option) =>
            values.studentCourse.some(
              (course: any) => course.id === option.value,
            ),
          )}
          onChange={handleCoursesChange}
        />
        <Button title="Create" isDisabled={isSubmitting} />
      </form>
    </>
  );
};
