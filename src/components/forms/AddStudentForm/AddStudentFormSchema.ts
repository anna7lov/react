import * as Yup from 'yup';

export const addStudentFormSchema = Yup.object().shape({
  studentName: Yup.string()
    .max(25, 'Max length 25 characters')
    .required('Required'),

  studentSurname: Yup.string()
    .max(25, 'Max length 25 characters')
    .required('Required'),

  studentEmail: Yup.string()
    .email('Please enter a valid email')
    .required('Required'),

  studentAge: Yup.number().required('Required').min(14).max(99),
});

export type AddStudentFormSchema = Yup.InferType<typeof addStudentFormSchema>;
