import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import { signUpThunk } from '../../../redux/user/thunks';
import {
  selectUser,
  selectisUserSignUpFailed,
} from '../../../redux/user/selectors';
import { registrationFormSchema } from './RegistrationFormSchema';
import { AuthLayout } from '../../layouts/auth/AuthLayout/AuthLayout';
import { InputField } from '../../InputField/InputField';
import { Button } from '../../Button/Button';
import { Checkbox } from '../../Checkbox/Checkbox';
import { Error } from '../../Error/Error';

export interface RegistrationFormValues {
  registrationName: string;
  registrationSurname: string;
  registrationEmail: string;
  registrationPassword: string;
  registrationConfirmPassword: string;
}

export const RegistrationForm = (): JSX.Element => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const user = useSelector(selectUser);
  const useSignUpError = useSelector(selectisUserSignUpFailed);
  const [showPassword, setShowPassword] = useState(false);

  const handleCheckboxChange = () => {
    setShowPassword(!showPassword);
  };

  useEffect(() => {
    if (user) {
      navigate('/lectors');
    }
  }, [user]);

  const onRegistrationFormSubmit = async (
    values: RegistrationFormValues,
  ): Promise<void> => {
    dispatch(
      signUpThunk({
        name: values.registrationName,
        surname: values.registrationSurname,
        email: values.registrationEmail,
        password: values.registrationPassword,
        passwordConfirmation: values.registrationConfirmPassword,
      }),
    );
  };

  const {
    values,
    errors,
    touched,
    isSubmitting,
    handleBlur,
    handleChange,
    handleSubmit,
  } = useFormik<RegistrationFormValues>({
    initialValues: {
      registrationName: '',
      registrationSurname: '',
      registrationEmail: '',
      registrationPassword: '',
      registrationConfirmPassword: '',
    },
    validationSchema: registrationFormSchema,
    onSubmit: onRegistrationFormSubmit,
  });

  return (
    <AuthLayout title="Register your account">
      <form noValidate onSubmit={handleSubmit}>
        <InputField
          label="Name"
          type="text"
          id="registration-name"
          name="registrationName"
          placeholder="Your Name"
          value={values.registrationName}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.registrationName}
          touched={touched.registrationName}
        />
        <InputField
          label="Surname"
          type="text"
          id="registration-surname"
          name="registrationSurname"
          placeholder="Your Surname"
          value={values.registrationSurname}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.registrationSurname}
          touched={touched.registrationSurname}
        />
        <InputField
          label="Email"
          type="email"
          id="registration-email"
          name="registrationEmail"
          placeholder="name@mail.com"
          value={values.registrationEmail}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.registrationEmail}
          touched={touched.registrationEmail}
        />
        <InputField
          label="Password"
          type={showPassword ? 'text' : 'password'}
          id="registration-password"
          name="registrationPassword"
          placeholder="******"
          value={values.registrationPassword}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.registrationPassword}
          touched={touched.registrationPassword}
        />
        <InputField
          label="Confirm Password"
          type={showPassword ? 'text' : 'password'}
          id="registration-confirm-password"
          name="registrationConfirmPassword"
          placeholder="******"
          value={values.registrationConfirmPassword}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.registrationConfirmPassword}
          touched={touched.registrationConfirmPassword}
        />
        <Checkbox
          label="Show Password"
          id="registration-checkbox"
          isChecked={showPassword}
          handleChange={handleCheckboxChange}
        />
        {useSignUpError ? <Error title={useSignUpError} /> : null}
        <Button title="Register" isDisabled={isSubmitting} />
      </form>
    </AuthLayout>
  );
};
