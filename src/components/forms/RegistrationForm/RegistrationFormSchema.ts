import * as Yup from 'yup';
import { RegistrationFormValues } from './RegistrationForm';

export const registrationFormSchema: Yup.Schema<RegistrationFormValues> =
  Yup.object().shape({
    registrationName: Yup.string()
      .max(25, 'Max length 25 characters')
      .required('Required'),

    registrationSurname: Yup.string()
      .max(25, 'Max length 25 characters')
      .required('Required'),

    registrationEmail: Yup.string()
      .email('Please enter a valid email')
      .max(25, 'Max length 25 characters')
      .required('Required'),

    registrationPassword: Yup.string()
      .min(6, 'Min length 6 characters')
      .required('Required'),

    registrationConfirmPassword: Yup.string()
      .oneOf(
        [Yup.ref('registrationPassword'), undefined],
        'Passwords must match',
      )
      .required('Required'),
  });
