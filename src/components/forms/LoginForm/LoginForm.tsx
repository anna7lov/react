import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import { signInThunk } from '../../../redux/user/thunks';
import {
  selectUser,
  selectisUserSignInFailed,
} from '../../../redux/user/selectors';
import { loginFormSchema } from './LoginFormSchema';
import { Button } from '../../Button/Button';
import { Error } from '../../Error/Error';
import { Checkbox } from '../../Checkbox/Checkbox';
import { InputField } from '../../InputField/InputField';
import { LinkItem } from '../../LinkItem/LinkItem';
import { AuthLayout } from '../../layouts/auth/AuthLayout/AuthLayout';
import styles from '../../../styles/shared.module.scss';
export interface LoginFormValues {
  loginEmail: string;
  loginPassword: string;
}

export const LoginForm = (): JSX.Element => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const user = useSelector(selectUser);
  const userSignInError = useSelector(selectisUserSignInFailed);
  const [showPassword, setShowPassword] = useState(false);

  const handleCheckboxChange = () => {
    setShowPassword(!showPassword);
  };

  useEffect(() => {
    if (user) {
      navigate('/lectors');
    }
  }, [user]);

  const onLoginFormSubmit = async (values: LoginFormValues): Promise<void> => {
    dispatch(
      signInThunk({
        email: values.loginEmail,
        password: values.loginPassword,
      }),
    );
  };

  const {
    values,
    errors,
    touched,
    isSubmitting,
    handleBlur,
    handleChange,
    handleSubmit,
  } = useFormik<LoginFormValues>({
    initialValues: {
      loginEmail: '',
      loginPassword: '',
    },
    validationSchema: loginFormSchema,
    onSubmit: onLoginFormSubmit,
  });

  return (
    <AuthLayout title="Welcome!">
      <form className={styles.formContainer} noValidate onSubmit={handleSubmit}>
        <InputField
          label="Email"
          type="email"
          id="login-email"
          name="loginEmail"
          placeholder="name@mail.com"
          value={values.loginEmail}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.loginEmail}
          touched={touched.loginEmail}
        />
        <InputField
          label="Password"
          type={showPassword ? 'text' : 'password'}
          id="login-password"
          name="loginPassword"
          placeholder="******"
          value={values.loginPassword}
          handleChange={handleChange}
          handleBlur={handleBlur}
          error={errors.loginPassword}
          touched={touched.loginPassword}
        />
        <Checkbox
          label="Show Password"
          id="login-checkbox"
          isChecked={showPassword}
          handleChange={handleCheckboxChange}
        />
        {userSignInError ? <Error title={userSignInError} /> : null}
        <Button title="Login" isDisabled={isSubmitting} />
      </form>
      <LinkItem
        address="/forgot-password"
        title="Forgot Password?"
        variant="text"
      />
      <LinkItem address="/sign-up" title="Register Now" variant="text" />
    </AuthLayout>
  );
};
