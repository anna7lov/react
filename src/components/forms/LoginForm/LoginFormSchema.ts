import * as Yup from 'yup';
import { LoginFormValues } from './LoginForm';

export const loginFormSchema: Yup.Schema<LoginFormValues> = Yup.object().shape({
  loginEmail: Yup.string()
    .email('Please enter a valid email')
    .required('Required'),

  loginPassword: Yup.string().required('Required'),
});
