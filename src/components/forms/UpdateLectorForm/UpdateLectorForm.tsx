import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { useFormik } from 'formik';
import { AxiosError } from 'axios';
import { updateLectorApi } from '../../../api/lectors.api';
import {
  selectLector,
  selectIsLectorLoading,
  selectLectorError,
  selectPage,
} from '../../../redux/lectors/selectors';
import {
  getAllLectorsThunk,
  getLectorThunk,
} from '../../../redux/lectors/thunks';
import { updateLectorFormSchema } from './UpdateLectorFormSchema';
import { Error } from '../../Error/Error';
import { Button } from '../../Button/Button';
import { InputField } from '../../InputField/InputField';
import { ToastContainer, toast } from 'react-toastify';
import { Loading } from '../../Loading/Loading';

export interface UpdateLectorFormValues {
  lectorName: string;
  lectorSurname: string;
  lectorEmail: string;
  lectorPassword?: string;
}

export interface UpdateLectorFormProps {
  lectorId: number;
  onClose: () => void;
}

export const UpdateLectorForm = ({
  lectorId,
  onClose,
}: UpdateLectorFormProps): JSX.Element => {
  const dispatch = useDispatch();
  const lector = useSelector(selectLector);
  const isLectorLoading = useSelector(selectIsLectorLoading);
  const lectorError = useSelector(selectLectorError);
  const page = useSelector(selectPage);

  useEffect(() => {
    dispatch(getLectorThunk(lectorId));
  }, [lectorId]);

  useEffect(() => {
    if (lector) {
      setValues({
        lectorName: lector.name,
        lectorSurname: lector.surname,
        lectorEmail: lector.email,
        lectorPassword: '',
      });
    }
  }, [lector]);

  const onUpdateLectorFormSubmit = async (
    values: UpdateLectorFormValues,
  ): Promise<void> => {
    try {
      await updateLectorApi({
        id: lector.id,
        name: values.lectorName,
        surname: values.lectorSurname,
        email: values.lectorEmail,
        password: values.lectorPassword,
      });
      dispatch(getAllLectorsThunk(page));
      onClose();
    } catch (error) {
      const errorMessage =
        error instanceof AxiosError
          ? error.response?.data.message || 'Something went wrong'
          : 'Something went wrong';

      toast.error(errorMessage, {
        position: toast.POSITION.TOP_CENTER,
        autoClose: false,
      });
    }
  };

  const {
    values,
    errors,
    touched,
    isSubmitting,
    handleBlur,
    handleChange,
    handleSubmit,
    setValues,
  } = useFormik<UpdateLectorFormValues>({
    initialValues: {
      lectorName: '',
      lectorSurname: '',
      lectorEmail: '',
      lectorPassword: '',
    },
    validationSchema: updateLectorFormSchema,
    onSubmit: onUpdateLectorFormSubmit,
  });

  return (
    <>
      <ToastContainer />
      {isLectorLoading ? (
        <Loading />
      ) : lectorError ? (
        <Error title={lectorError} />
      ) : (
        <form noValidate onSubmit={handleSubmit}>
          <InputField
            label="Name"
            type="text"
            id="lector-name"
            name="lectorName"
            placeholder="Name"
            value={values.lectorName || ''}
            handleChange={handleChange}
            handleBlur={handleBlur}
            error={errors.lectorName}
            touched={touched.lectorName}
          />
          <InputField
            label="Surname"
            type="text"
            id="lector-surname"
            name="lectorSurname"
            placeholder="Surname"
            value={values.lectorSurname || ''}
            handleChange={handleChange}
            handleBlur={handleBlur}
            error={errors.lectorSurname}
            touched={touched.lectorSurname}
          />
          <InputField
            label="Email"
            type="email"
            id="lector-email"
            name="lectorEmail"
            placeholder="name@mail.com"
            value={values.lectorEmail || ''}
            handleChange={handleChange}
            handleBlur={handleBlur}
            error={errors.lectorEmail}
            touched={touched.lectorEmail}
          />
          <InputField
            label="Password"
            type="text"
            id="lector-password"
            name="lectorPassword"
            placeholder="******"
            value={values.lectorPassword || ''}
            handleChange={handleChange}
            handleBlur={handleBlur}
            error={errors.lectorPassword}
            touched={touched.lectorPassword}
          />
          <Button title="Update" isDisabled={isSubmitting} />
        </form>
      )}
    </>
  );
};
