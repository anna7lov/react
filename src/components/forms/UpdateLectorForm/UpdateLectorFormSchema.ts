import * as Yup from 'yup';
import { UpdateLectorFormValues } from './UpdateLectorForm';

export const updateLectorFormSchema: Yup.Schema<UpdateLectorFormValues> =
  Yup.object().shape({
    lectorName: Yup.string()
      .max(25, 'Max length 25 characters')
      .required('Required'),

    lectorSurname: Yup.string()
      .max(25, 'Max length 25 characters')
      .required('Required'),

    lectorEmail: Yup.string()
      .email('Please enter a valid email')
      .required('Required'),

    lectorPassword: Yup.string().min(6, 'Min length 6 characters').optional(),
  });
