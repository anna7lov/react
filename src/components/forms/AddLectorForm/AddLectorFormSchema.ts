import * as Yup from 'yup';
import { AddLectorFormValues } from './AddLectorForm';

export const addLectorFormSchema: Yup.Schema<AddLectorFormValues> =
  Yup.object().shape({
    lectorName: Yup.string()
      .max(25, 'Max length 25 characters')
      .required('Required'),

    lectorSurname: Yup.string()
      .max(25, 'Max length 25 characters')
      .required('Required'),

    lectorEmail: Yup.string()
      .email('Please enter a valid email')
      .required('Required'),

    lectorPassword: Yup.string()
      .min(6, 'Min length 6 characters')
      .required('Required'),
  });
