import { useFormik } from 'formik';
import { InputField } from '../../InputField/InputField';
import { Button } from '../../Button/Button';
import { addLectorApi } from '../../../api/lectors.api';
import { AxiosError } from 'axios';
import { toast } from 'react-toastify';
import { addLectorFormSchema } from './AddLectorFormSchema';
import { useDispatch, useSelector } from 'react-redux';
import { selectPage } from '../../../redux/lectors/selectors';
import { getAllLectorsThunk } from '../../../redux/lectors/thunks';

export interface AddLectorFormValues {
  lectorName: string;
  lectorSurname: string;
  lectorEmail: string;
  lectorPassword: string;
}

export const AddLectorForm = (): JSX.Element => {
  const dispatch = useDispatch();
  const page = useSelector(selectPage);

  const onLectorFormSubmit = async (
    values: AddLectorFormValues,
  ): Promise<void> => {
    try {
      await addLectorApi({
        name: values.lectorName,
        surname: values.lectorSurname,
        email: values.lectorEmail,
        password: values.lectorPassword,
      });
      dispatch(getAllLectorsThunk(page));
    } catch (error) {
      const errorMessage =
        error instanceof AxiosError
          ? error.response?.data.message || 'Something went wrong'
          : 'Something went wrong';

      console.log(errorMessage);

      toast.error(errorMessage, {
        position: toast.POSITION.TOP_CENTER,
        autoClose: false,
      });
    }
  };

  const {
    values,
    errors,
    touched,
    isSubmitting,
    handleBlur,
    handleChange,
    handleSubmit,
  } = useFormik<AddLectorFormValues>({
    initialValues: {
      lectorName: '',
      lectorSurname: '',
      lectorEmail: '',
      lectorPassword: '',
    },
    validationSchema: addLectorFormSchema,
    onSubmit: onLectorFormSubmit,
  });

  return (
    <form noValidate onSubmit={handleSubmit}>
      <InputField
        label="Name"
        type="text"
        id="lector-name"
        name="lectorName"
        placeholder="Name"
        value={values.lectorName}
        handleChange={handleChange}
        handleBlur={handleBlur}
        error={errors.lectorName}
        touched={touched.lectorName}
      />
      <InputField
        label="Surname"
        type="text"
        id="lector-surname"
        name="lectorSurname"
        placeholder="Surname"
        value={values.lectorSurname}
        handleChange={handleChange}
        handleBlur={handleBlur}
        error={errors.lectorSurname}
        touched={touched.lectorSurname}
      />
      <InputField
        label="Email"
        type="email"
        id="lector-email"
        name="lectorEmail"
        placeholder="name@mail.com"
        value={values.lectorEmail}
        handleChange={handleChange}
        handleBlur={handleBlur}
        error={errors.lectorEmail}
        touched={touched.lectorEmail}
      />
      <InputField
        label="Password"
        type="text"
        id="lector-password"
        name="lectorPassword"
        placeholder="******"
        value={values.lectorPassword}
        handleChange={handleChange}
        handleBlur={handleBlur}
        error={errors.lectorPassword}
        touched={touched.lectorPassword}
      />
      <Button title="Create" isDisabled={isSubmitting} />
    </form>
  );
};
