import styles from './AddButton.module.scss';

interface AddButtonProps {
  title: string;
  onAddClicked: () => void;
}

export const AddButton = ({
  title,
  onAddClicked,
}: AddButtonProps): JSX.Element => {
  return (
    <button type="button" onClick={onAddClicked} className={styles.button}>
      <span className={styles.addItem}>Add new {title}</span>
    </button>
  );
};
