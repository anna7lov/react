import { StylesConfig } from 'react-select';

export const selectCustomStyles: StylesConfig = {
  control: (base, state) => ({
    ...base,
    '&:hover': { borderColor: 'none' },
    boxShadow: 'none',
    height: '38px',
    borderRadius: '6px',
    border: '1px solid #e4e7ed',
    ...(state.menuIsOpen && {
      borderBottom: '1px solid #ffffff',
      borderBottomLeftRadius: 0,
      borderBottomRightRadius: 0,
    }),
  }),
  menu: (base) => ({
    ...base,
    margin: '0',
    boxShadow: 'none',
    border: '1px solid #e4e7ed',
    borderTop: '1px solid #ffffff',
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    '&::before': {
      top: '-1px',
      left: '12px',
      content: '""',
      width: '114px',
      height: '1px',
      backgroundColor: '#e4e7ed',
      position: 'absolute',
    },
  }),
  option: (base, state) => ({
    ...base,
    height: '24px',
    padding: '0 12px',
    fontFamily: 'Nunito',
    fontSize: '14px',
    fontStyle: 'normal',
    fontWeight: 500,
    lineHeight: '24px',
    opacity: state.isSelected ? 1 : 0.45,
    color: '#3B4360',
    backgroundColor: 'none',
    cursor: 'pointer',
    '&:not(:last-child)': { marginBottom: '8px' },
    '&:hover': {
      color: state.isSelected ? '#3B4360' : '#3333ff',
      opacity: 1,
    },
  }),
  menuList: (base) => ({
    ...base,
    padding: '10px 0 5px',
  }),
};
