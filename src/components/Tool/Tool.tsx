import Select, { DropdownIndicatorProps, components } from 'react-select';
import { useState } from 'react';
import { selectCustomStyles } from './selectCustomStyles';
import styles from './Tool.module.scss';

const DropdownIndicator = (props: DropdownIndicatorProps) => {
  return (
    <components.DropdownIndicator {...props}>
      {props.selectProps.menuIsOpen ? (
        <svg
          width="24"
          height="24"
          viewBox="0 0 24 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M16.5303 14.5303C16.2374 14.8232 15.7626 14.8232 15.4697 14.5303L12 11.0607L8.53033 14.5303C8.23744 14.8232 7.76256 14.8232 7.46967 14.5303C7.17678 14.2374 7.17678 13.7626 7.46967 13.4697L11.4697 9.46967C11.7626 9.17678 12.2374 9.17678 12.5303 9.46967L16.5303 13.4697C16.8232 13.7626 16.8232 14.2374 16.5303 14.5303Z"
            fill="#3B4360"
          />
        </svg>
      ) : (
        <svg
          width="24"
          height="24"
          viewBox="0 0 24 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M7.46967 9.46967C7.76256 9.17678 8.23744 9.17678 8.53033 9.46967L12 12.9393L15.4697 9.46967C15.7626 9.17678 16.2374 9.17678 16.5303 9.46967C16.8232 9.76256 16.8232 10.2374 16.5303 10.5303L12.5303 14.5303C12.2374 14.8232 11.7626 14.8232 11.4697 14.5303L7.46967 10.5303C7.17678 10.2374 7.17678 9.76256 7.46967 9.46967Z"
            fill="#3B4360"
          />
        </svg>
      )}
    </components.DropdownIndicator>
  );
};

interface ToolProps {
  sortOptions: { value: string; label: string }[];
  searchOptions: { value: string; label: string }[];
  onNameChange: (newName: string) => void;
  onSortChange: (newSort: string, newOrder: string) => void;
  onSearchChange: (newSearch: string) => void;
  filter: { sort: string; order: string; query: string; search: string };
}

export const Tool = ({
  sortOptions,
  searchOptions,
  onNameChange,
  onSortChange,
  onSearchChange,
  filter,
}: ToolProps): JSX.Element => {
  const [inputText, setInputText] = useState(filter.query);

  const handleSortChange = (selectedSortOption: any) => {
    if (selectedSortOption && selectedSortOption.value === 'all') {
      onSortChange('', '');
    } else if (selectedSortOption) {
      const [sort, order] = selectedSortOption.value.split('-');
      onSortChange(sort, order);
    }
  };

  const handleSearchChange = (selectedSearchOption: any) => {
    onSearchChange(selectedSearchOption.value);
  };

  const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputText(e.target.value);
  };

  const onSearchFormSubmit = (e: React.ChangeEvent<HTMLFormElement>) => {
    e.preventDefault();
    onNameChange(inputText);
  };

  return (
    <div className={styles.tools}>
      <div className={styles.tools__sort}>
        <label htmlFor="sort-select" className={styles.select__label}>
          Sort by
        </label>
        <Select
          id="sort-select"
          styles={selectCustomStyles}
          options={sortOptions}
          value={sortOptions.find(
            (option) => option.value === `${filter.sort}-${filter.order}`,
          )}
          className={styles.select}
          components={{ DropdownIndicator, IndicatorSeparator: null }}
          onChange={handleSortChange}
        />
      </div>
      <div className={styles.tools__sort}>
        <label htmlFor="search-select" className={styles.select__label}>
          Search by
        </label>
        <Select
          id="search-select"
          styles={selectCustomStyles}
          options={searchOptions}
          value={searchOptions.find((option) => option.value === filter.search)}
          className={styles.select}
          components={{ DropdownIndicator, IndicatorSeparator: null }}
          onChange={handleSearchChange}
        />
      </div>
      <form className={styles.tools__searchForm} onSubmit={onSearchFormSubmit}>
        <input
          placeholder="Search"
          type="search"
          value={inputText}
          onChange={onInputChange}
          className={styles.tools__input}
        />
        <svg
          width="16"
          height="16"
          viewBox="0 0 16 16"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M7.66671 14.5C3.90004 14.5 0.833374 11.4333 0.833374 7.66668C0.833374 3.90001 3.90004 0.833344 7.66671 0.833344C11.4334 0.833344 14.5 3.90001 14.5 7.66668C14.5 11.4333 11.4334 14.5 7.66671 14.5ZM7.66671 1.83334C4.44671 1.83334 1.83337 4.45334 1.83337 7.66668C1.83337 10.88 4.44671 13.5 7.66671 13.5C10.8867 13.5 13.5 10.88 13.5 7.66668C13.5 4.45334 10.8867 1.83334 7.66671 1.83334Z"
            fill="#646B75"
          />
          <path
            d="M14.6666 15.1667C14.54 15.1667 14.4133 15.12 14.3133 15.02L12.98 13.6867C12.7866 13.4933 12.7866 13.1733 12.98 12.98C13.1733 12.7867 13.4933 12.7867 13.6866 12.98L15.02 14.3133C15.2133 14.5067 15.2133 14.8267 15.02 15.02C14.92 15.12 14.7933 15.1667 14.6666 15.1667Z"
            fill="#646B75"
          />
        </svg>
      </form>
    </div>
  );
};
