import cn from 'classnames';
import logo from '../../assets/header_logo.svg';
import avatar from '../../assets/avatar.png';
import styles from './Header.module.scss';

interface HeaderProps {
  title?: string;
  isLogo?: boolean;
}

export const Header = ({ title, isLogo }: HeaderProps): JSX.Element => {
  return (
    <header
      className={cn(styles.header, {
        [styles.gray]: !isLogo,
      })}
    >
      {isLogo ? (
        <div className={styles.header__logo}>
          <img
            src={logo}
            alt="University logo"
            className={styles.header__logoImage}
          />
        </div>
      ) : (
        <h1 className={styles.header__title}>{title}</h1>
      )}
      <div className={styles.header__avatarWrapper}>
        <img src={avatar} alt="User avatar" className={styles.header__avatar} />
      </div>
    </header>
  );
};
