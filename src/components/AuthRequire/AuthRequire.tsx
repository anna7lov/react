import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';
import { selectIsUserLoading, selectUser } from '../../redux/user/selectors';
import { Loading } from '../Loading/Loading';

interface AuthRequireProps {
  children: React.ReactElement;
}

export const AuthRequire = ({
  children,
}: AuthRequireProps): JSX.Element | null => {
  const user = useSelector(selectUser);
  const isUserLoading = useSelector(selectIsUserLoading);

  if (isUserLoading) {
    return <Loading />;
  }

  return user ? children : <Navigate to="/" />;
};
