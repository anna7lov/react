import cn from 'classnames';
import styles from './InputField.module.scss';

interface InputProps {
  label: string;
  type: string;
  id: string;
  name: string;
  placeholder: string;
  value: string;
  handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  handleBlur: (e: React.ChangeEvent<HTMLInputElement>) => void;
  error: string | undefined;
  touched: boolean | undefined;
}

export const InputField = ({
  label,
  type,
  id,
  name,
  placeholder,
  value,
  handleChange,
  handleBlur,
  error,
  touched,
}: InputProps): JSX.Element => {
  return (
    <div className={styles.field}>
      <label htmlFor={id} className={styles.field__label}>
        {label}
      </label>
      <input
        required
        type={type}
        id={id}
        name={name}
        placeholder={placeholder}
        autoComplete="on"
        value={value}
        onChange={handleChange}
        onBlur={handleBlur}
        className={cn(styles.field__input, {
          [styles.error]: error && touched,
        })}
      />
      {error && touched ? <p className={styles.field__error}>{error}</p> : null}
    </div>
  );
};
