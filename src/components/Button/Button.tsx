import styles from './Button.module.scss';

interface ButtonProps {
  title: string;
  isDisabled?: boolean;
}

export const Button = ({ title, isDisabled }: ButtonProps): JSX.Element => {
  return (
    <button type="submit" disabled={isDisabled} className={styles.button}>
      {title}
    </button>
  );
};
