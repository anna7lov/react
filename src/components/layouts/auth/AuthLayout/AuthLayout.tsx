import { ReactNode } from 'react';
import cn from 'classnames';
import { Logo } from '../../../Logo/Logo';
import styles from './AuthLayout.module.scss';

interface LayoutProps {
  title: string;
  paragraph?: string;
  paragraphCenter?: boolean;
  children: ReactNode;
}

export const AuthLayout = ({
  title,
  paragraph,
  paragraphCenter,
  children,
}: LayoutProps): JSX.Element => {
  return (
    <div className={styles.layout}>
      <div className={styles.layout__top}>
        <Logo />
        <h1 className={styles.layout__title}>{title}</h1>
        {paragraph ? (
          <p
            className={cn(styles.layout__paragraph, {
              [styles.center]: paragraphCenter,
            })}
          >
            {paragraph}
          </p>
        ) : null}
      </div>
      {children}
    </div>
  );
};
