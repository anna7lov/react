export interface NavLink {
  id: number;
  address: string;
  title: string;
  additionalClass: string;
}

export const navLinksList: NavLink[] = [
  {
    id: 1,
    address: '/courses',
    title: 'Courses',
    additionalClass: 'icon-course',
  },
  {
    id: 2,
    address: '/lectors',
    title: 'Lectors',
    additionalClass: 'icon-lector',
  },
  {
    id: 3,
    address: '/groups',
    title: 'Groups',
    additionalClass: 'icon-group',
  },
  {
    id: 4,
    address: '/students',
    title: 'Students',
    additionalClass: 'icon-student',
  },
];
