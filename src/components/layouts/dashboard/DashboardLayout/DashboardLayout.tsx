import { ReactNode } from 'react';
import cn from 'classnames';
import logo from '../../../../assets/dashboard_logo.svg';
import styles from './DashboardLayout.module.scss';
import { NavLink } from 'react-router-dom';
import { navLinksList } from './navLinksList';
import { useDispatch } from 'react-redux';
import { logOutAction } from '../../../../redux/user/actions';
import { Header } from '../../../Header/Header';

interface DashboardLayoutProps {
  title: string;
  children: ReactNode;
}

export const DashboardLayout = ({
  title,
  children,
}: DashboardLayoutProps): JSX.Element => {
  const dispatch = useDispatch();

  const onLogoutClicked = () => {
    localStorage.removeItem('token');
    dispatch(logOutAction());
  };

  return (
    <div className={styles.dashboard}>
      <aside className={styles.dashboard__aside}>
        <div>
          <div className={styles.dashboard__asideTop}>
            <div className={styles.dashboard__logo}>
              <img
                src={logo}
                alt="University logo"
                className={styles.dashboard__logoImage}
              />
            </div>
          </div>
          <nav className={styles.dashboard__nav}>
            <ul>
              {navLinksList.map((navLink) => (
                <li key={navLink.id} className={styles.dashboard__item}>
                  <NavLink
                    to={navLink.address}
                    className={({ isActive }) =>
                      cn(styles.dashboard__link, {
                        [styles.active]: isActive,
                      })
                    }
                  >
                    <span
                      className={cn(
                        styles.dashboard__linkText,
                        styles[navLink.additionalClass],
                      )}
                    >
                      {navLink.title}
                    </span>
                  </NavLink>
                </li>
              ))}
            </ul>
          </nav>
        </div>
        <button
          onClick={onLogoutClicked}
          className={styles.dashboard__logoutButton}
        >
          <span className={styles.dashboard__logoutItem}>Log out</span>
        </button>
      </aside>

      <main className={styles.dashboard__main}>
        <Header title={title} />
        <div className={styles.dashboard__mainContent}>{children}</div>
      </main>
    </div>
  );
};
