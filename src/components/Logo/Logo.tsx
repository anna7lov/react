import logo from '../../assets/logo.svg';
import styles from './Logo.module.scss';

export const Logo = (): JSX.Element => {
  return (
    <div className={styles.logo}>
      <img src={logo} alt="University logo" className={styles.logo__image} />
    </div>
  );
};
